/* SPDX-License-Identifier: GPL-2.0 */
/*
 * V4L2 interface header
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2019 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef _VXE_V4L2_H
#define _VXE_V4L2_H

#include "osal/inc/osa_define.h"

/*
 * struct vxe_ctrl - contains info for each supported v4l2 control
 */
struct vxe_ctrl {
	uint32 cid;
	enum v4l2_ctrl_type type;
	uint8 name[32];
	int32 minimum;
	int32 maximum;
	int32 step;
	int32 default_value;
	osa_bool compound;
};

#endif
