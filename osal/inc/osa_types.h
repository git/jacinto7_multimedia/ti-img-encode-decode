/* SPDX-License-Identifier: GPL-2.0 */
/*
 * osa data type definitions
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef OSA_TYPES_H_
#define OSA_TYPES_H_

/* This structure definition used for circular linked list implementation */
struct clist_head {
    struct clist_head *next, *prev;
};

enum {
    osa_false   = 0,
    osa_true    = 1,
	OSA_STATE_FORCE32BITS = 0x7FFFFFFFU
};

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef NULL
#define NULL ((void *)0)
#endif

enum {
    HWA_DECODER   = 0,
    HWA_ENCODER    = 1,
    HWA_FORCE32BITS = 0x7FFFFFFFU
};

#define MAX_PLANES	3

#if defined(OSAL_LINUX)
/* ---------- Intel/AMD x64 processor family -------------------------*/
  typedef signed char                           int8;
  typedef short int                             int16;
  typedef int                                   int32;
  typedef long long int                         int64;
  typedef unsigned char                         uint8;
  typedef unsigned short int                    uint16;
  typedef unsigned int                          uint32;
  typedef unsigned long long int                uint64;
  typedef unsigned long long int                uint_addr;

  typedef unsigned char                         osa_char;

  typedef long                              	slong;
  typedef unsigned long                     	ulong;

  typedef unsigned long int                     uintptr_t;
  typedef long unsigned int                     size_t;

  typedef unsigned char                         osa_bool;

  typedef struct v4l2_fh			file_hndl;
  typedef struct v4l2_m2m_buffer		m2m_buffer;
  typedef struct v4l2_m2m_dev			m2m_dev;
  typedef struct v4l2_device			ti_device;

/* ---------- Intel/AMD x64 processor family -------------------------*/
#elif defined(OSAL_SYSBIOS)
/* ---------- For J7 R5F ARM32 Processor family ----------------------*/
	#include <stdio.h>
	#include <stdint.h>
	#include <string.h>

	typedef   signed char   int8;
	typedef          char   uint8;
	typedef          short  int16;
	typedef unsigned short  uint16;
	typedef          int    int32;
	typedef unsigned int    uint32;
	typedef          long long int64;
	typedef unsigned long long uint64;
	typedef unsigned int        uint_addr;

	typedef long slong;
	typedef char     osa_char;

	typedef unsigned char osa_bool;

#ifndef true
#define true 1
#endif

#ifndef false
#define false 0
#endif

#if !defined(A72)
    typedef unsigned long ulong;

struct timespec{
	uint64 tv_sec;
	uint64 tv_nsec;
};
#endif

struct scatterlist{
	unsigned long	page_link;
	unsigned int	offset;
	unsigned int	length;
	void            *dma_address;
	uint32          is_alloc_inside;
};

struct sg_table{
	struct scatterlist *sgl;       /* the list */
	unsigned int       nents;      /* number of mapped entries */
	unsigned int       orig_nents; /* original size of list */
};

struct page {
	struct sg_table   *sgt;
};

struct work_struct{
	void (* worker_func)(void *work);
	void *work;
	uint8 work_complete;
	void *timer_handle;
	uint32 timer_status;
	void *queue_handle;
	void *workq_sem_handle;
	void *lock;
};

struct device{
	void *phy_addr;
};

struct v4l2_device{
	struct device   *dev;
};

typedef void*				file_hndl;
typedef void*				m2m_dev;
typedef struct mm_buffer*	m2m_buffer;
typedef struct v4l2_device	ti_device;

/* ---------- For J7 R5F ARM32 Processor family ----------------------*/
#else
#error "Define macro for othe paltform"
#endif

#endif /* OSA_TYPES_H_ */
