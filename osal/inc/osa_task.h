/* SPDX-License-Identifier: GPL-2.0 */
/*
 * osa definitions header
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef OSA_TASK_H_
#define OSA_TASK_H_

#include "osa_types.h"

enum task_priority {
	OSA_INTERRUPT_TASK_PRIORITY,
	OSA_VXD_WORKER_TASK_PRIORITY,
	OSA_STREAM_WORKER_TASK_PRIORITY
};

typedef struct task_params_s
{
    uint8 *name;           			/*!< Name of the task instance.                  */
    enum task_priority priority;    /*!< The priority of the task                    */
    uint32 stacksize;   			/*!< The stack size of the task                  */
    void *arg0;           			/*!< arg0                                        */
    void *arg1;           			/*!< arg1                                        */
    void *stack;       				/*!< pointer to stack memory, if NULL OS will allocate internally */
} task_params;

/*!
 *  @brief  Function to create a task.
 *
 *  @param  taskfxn  Function pointer of the task.
 *
 *  @param  params  Pointer to the instance configuration parameters.
 *
 *  @return A handler on success or a NULL on an error
 */
void *osa_task_create(void *taskfxn, task_params *params);

/*!
 *  @brief  Function to delete a task.
 *
 *  @param  handle  A handle returned from osa_task_create
 *
 *  @return Status of the functions
 *    - 0: Deleted the task instance
 *    - -ve value: Failed to delete the task instance
 */
int32 osa_task_delete(void **handle);

/*!
 *  @brief  Function for Task sleep in units ms-second
 *
 *  @param timeout  sleep ticks.
 */
void osa_task_sleep(uint32 timeout);

#endif /* OSA_TASK_H_ */
