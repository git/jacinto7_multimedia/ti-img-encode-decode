/* SPDX-License-Identifier: GPL-2.0 */
/*
 * osa string management defintions
 *
 * Copyright (c) 2019 Texas Instruments Incorporated - http://www.ti.com/
 */

#ifndef OSA_STRING_H_
#define OSA_STRING_H_

#include "osa_types.h"

/**
 * osa_memcpy - copy memory from src to dst
 * @dst - destination adress
 * @src - source address
 * @bytes - size in bytes to copy
 *
 * Returns a pointer to dst
 */
void *osa_memcpy(void *dst, void *src, uint32 bytes);

/**
 * osa_memset - fill memory with specified value
 * @ptr - address of memory
 * @val - value to use to fill memory
 * @bytes - size in bytes to fill
 *
 * Returns a pointer to address filled
 */
void *osa_memset(void *ptr, uint32 val, uint32 bytes);

/**
 * osa_memmove - copy memory area
 *
 * @dst - destination area
 * @src - source area
 * @bytes - bytes to move
 *
 * Returns a pointer to dst
 */
void *osa_memmove(void *dst, void *src, uint32 bytes);

/**
 * osa_strcmp - compare two strings
 * @str1 - address of string 1 to compare
 * @str2 - address of string 2 to compare
 *
 * Returns 0 if the strings match, and non-zero value
 * if they do not match.
 */
int osa_strcmp(const osa_char *str1, const osa_char *str2);

/**
 * osa_strncmp - compare two strings for n bytes
 * @str1 - address of string 1
 * @str2 - address of string 2
 * @size - max size in bytes to compare
 *
 * Returns 0 if the strings match, and non-zero value
 * if they do not match.
 */
int osa_strncmp(const osa_char *str1, const osa_char *str2, uint32 size);

#endif /* OSA_STRING_H_ */
