/* SPDX-License-Identifier: GPL-2.0 */
/*
 * osa definitions header
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef OSA_DEFINE_H_
#define OSA_DEFINE_H_

#if defined(OSAL_LINUX)

#include <linux/dma-mapping.h>
#include <media/v4l2-ctrls.h>
#include <media/v4l2-device.h>
#include <media/v4l2-mem2mem.h>

#if defined (DEBUG_DECODER_DRIVER) || defined (DEBUG_ENCODER_DRIVER)
#define OSA_PR_INFO             pr_info
#define OSA_PR_DEBUG            pr_debug
#define OSA_DEV_INFO            dev_info
#define OSA_DEV_DBG             dev_dbg
#else
#define OSA_PR_INFO(msg...)
#define OSA_PR_DEBUG(msg...)
#define OSA_DEV_INFO(osa_dev, msg...)
#define OSA_DEV_DBG(osa_dev, msg...)
#endif
#define OSA_DEV_WARN            dev_warn
#define OSA_PR_WARN             pr_warn
#define OSA_DEV_ERR             dev_err
#define OSA_PR_ERR              pr_err
#define OSA_WARN_ON             WARN_ON
#define OSA_PAGE_SIZE           PAGE_SIZE
#define OSA_GFP_KERNEL          GFP_KERNEL
#define OSA_THIS_MODULE         THIS_MODULE
#define OSA_DMA_TO_DEVICE       DMA_TO_DEVICE
#define OSA_DMA_FROM_DEVICE     DMA_FROM_DEVICE
#define OSA_DMA_BIDIRECTIONAL   DMA_BIDIRECTIONAL
#define OSA_DUMP_STACK          dump_stack()

#define MAX_PLANES		3


#elif defined(OSAL_SYSBIOS)

#include "osa_types.h"
#include <ti/osal/TaskP.h>

#define ALIGN(val, alignv)  (((val) + ((typeof(val))(alignv) - 1)) & ~((typeof(val))((alignv)) - 1))
#define BIT(nr)             (1UL << (nr))
#define min(a, b)           (((a) < (b)) ? (a) : (b))
#define ARRAY_SIZE(x)       (sizeof(x) / sizeof((x)[0]))
#define likely(x)           __builtin_expect((x),1)
#define unlikely(x)         __builtin_expect((x),0)

/* PAZE_SIZE value will change according to architecture */
#define PAGE_SIZE               4096
#define GFP_KERNEL              0
#define THIS_MODULE             (void *)0
#define DMA_TO_DEVICE           0
#define DMA_BIDIRECTIONAL       0
#define DMA_FROM_DEVICE         0

#define NSEC_PER_SEC            (1000000000U)
#define MAX_PLANES              3

#define __iomem
#define HW_SYNC_BARRIER()

/**
 * \brief Align 64b value to 'align' bytes
 */
static inline uint64_t ALIGN64(uint64_t val, uint32_t align)
{
    return (uint64_t)( (uint64_t)(val+align-1) / align) * align;
}

/**
 * \brief Align 32b value to 'align' bytes
 */
static inline uint32_t ALIGN32(uint32_t val, uint32_t align)
{
    return (uint32_t)( (uint32_t)(val+align-1) / align) * align;
}

static inline uint32_t HW_RD_REG32_RAW(uint32_t addr)
{
    uint32_t regVal = *(volatile uint32_t *) ((uintptr_t) addr);
    /* Donot call any functions after this. If required implement as macros */
    HW_SYNC_BARRIER();
    return (regVal);
}

static inline void HW_WR_REG32_RAW(uint32_t addr, uint32_t value)
{
    *(volatile uint32_t *) ((uintptr_t) addr) = value;
    /* Donot call any functions after this. If required implement as macros */
    HW_SYNC_BARRIER();
    return;
}

#define ioread32(addr) (HW_RD_REG32_RAW((uint32_t) (addr)))

#define iowrite32(value, addr)    \
    (HW_WR_REG32_RAW((uint32_t) (addr), (uint32_t) (value)))

static inline int64 div_s64_rem(int64 dividend, int32 divisor, int32 *remainder)
{
	*remainder = dividend % divisor;
	return dividend / divisor;
}

static inline int64 div_s64(int64 dividend, int32 divisor)
{
	int32 remainder;
	return div_s64_rem(dividend, divisor, &remainder);
}

extern volatile int32 g_AssertFailLoop;

void VideoCodec_printf(const char *format, ...);

#ifdef DEBUG_DECODER_DRIVER
#define OSA_DEV_DBG(DEV, ...)     	VideoCodec_printf(__VA_ARGS__)
#define OSA_DEV_INFO(DEV, ...)     	VideoCodec_printf(__VA_ARGS__)
#define OSA_PR_DEBUG            	VideoCodec_printf
#define OSA_PR_INFO             	VideoCodec_printf
#else
#define OSA_PR_INFO(msg...)
#define OSA_PR_DEBUG(msg...)
#define OSA_DEV_INFO(osa_dev, msg...)
#define OSA_DEV_DBG(osa_dev, msg...)
#endif
#define OSA_WARN_ON             	UTILS_assert
#define OSA_PR_ERR              	VideoCodec_printf
#define OSA_PR_WARN             	VideoCodec_printf
#define OSA_DEV_ERR(DEV, ...)      	VideoCodec_printf(__VA_ARGS__)
#define OSA_DEV_WARN(DEV, ...)     	VideoCodec_printf(__VA_ARGS__)

#define OSA_PAGE_SIZE				PAGE_SIZE
#define OSA_GFP_KERNEL          	GFP_KERNEL
#define OSA_THIS_MODULE         	THIS_MODULE
#define OSA_DMA_TO_DEVICE       	DMA_TO_DEVICE
#define OSA_DMA_FROM_DEVICE     	DMA_FROM_DEVICE
#define OSA_DMA_BIDIRECTIONAL   	DMA_BIDIRECTIONAL
#define OSA_DUMP_STACK          	dump_stack()

#define UTILS_assert(y)                                     \
    (UTILS_assertLocal((osa_bool) (y), (const int8 *) # y, \
                       (const int8 *) __FILE__, (int32) __LINE__))

static inline void UTILS_assertLocal(osa_bool   condition,
                                     const int8  *str,
                                     const int8  *fileName,
                                     int32  lineNum)
{
	if (condition != FALSE) {
		OSA_PR_ERR(" Assertion @ Line: %d in %s: %s : failed !!!\n",
				lineNum, fileName, str);
		OSA_PR_ERR(" Assertion @ Line: %d in %s: %s : failed !!!\n",
				lineNum, fileName, str);
		while(g_AssertFailLoop)
		{
			TaskP_sleepInMsecs(1U);
		}
	}
	return;
}

static inline void dump_stack()
{
	/* This is dummy implementation, later we have change */
	UTILS_assert(TRUE);
}
#else

#error "Define macro for othe paltform"

#endif

#endif /* OSA_DEFINE_H_ */
