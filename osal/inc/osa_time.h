/* SPDX-License-Identifier: GPL-2.0 */
/*
 * osa time related functions definitions
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef OSA_TIME_H_
#define OSA_TIME_H_

#include "osa_types.h"

#define osa_time_before(a, b)                   ((int64)((a) - (b)) < 0)
#define osa_time_is_before_eq_jiffies(a, b)     ((int64)((a) - (b)) >= 0)
#define osa_time_is_after_jiffies(a, b)         ((int64)((a) - (b)) < 0)

#if defined(OSAL_LINUX)
#include <linux/time.h>

struct osa_timespec {
	struct timespec ts;
};
#elif defined(OSAL_SYSBIOS)
struct osa_timespec {
	uint64 ts;
};
#endif

/* This function will return reminder and quotient */
static inline uint32 osa_do_div(uint64 *n, uint32 base)
{
    uint32 remainder = *n % base;
    *n = *n / base;
    return remainder;
}

/* Jiffies is a global variable declared in <linux/jiffies.h>
 * as: extern unsigned long volatile jiffies; Its only usage is to store the
 * number of ticks occurred since system start-up. On kernel boot-up,
 * jiffies is initialized to a special initial value, and it is incremented
 * by one for each timer interrupt
 */

uint64 osa_get_number_of_ticks(void);

/**
 * osa_msecs_to_jiffies: - convert milliseconds to jiffies
 * @m:  time in milliseconds
 *
 * conversion is done as follows:
 *
 * - negative values mean 'infinite timeout' (MAX_JIFFY_OFFSET)
 *
 * - 'too large' values [that would result in larger than
 *   MAX_JIFFY_OFFSET values] mean 'infinite timeout' too.
 *
 * - all other values are converted to jiffies by either multiplying
 *   the input value by a factor or dividing it with a factor and
 *   handling any 32-bit overflows.
 */
uint64 osa_msecs_to_jiffies(uint32 m);

/*
 * Convert jiffies to milliseconds and back.
 */
uint64 osa_jiffies_to_msecs(uint64 j);

/* This function allocate memory and return current time */
void osa_getnstimeofday(struct osa_timespec *ts_args);

/* This function allocate memory return the time difference */
void osa_timespec_sub(struct osa_timespec *end_time, struct osa_timespec *start_time, struct osa_timespec *dif_time);

/**
 * osa_timespec_to_ns - Convert given time to nanoseconds
 * @ts_args: pointer to the variable, to be converted
 *
 * Returns the given time in nanosecond
 */
int64 osa_timespec_to_ns(struct osa_timespec *ts_args);

#endif /* OSA_TIME_H_ */
