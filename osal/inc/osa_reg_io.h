/* SPDX-License-Identifier: GPL-2.0 */
/*
 * osa register io definitions
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2019 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef OSA_REG_IO_H_
#define OSA_REG_IO_H_

#include "osa_types.h"

/**
 * osa_reg32_write - Write a value to a 32-bit register
 *
 * @val: 32-bit value to write to the register
 * @addr: Address of the register to write
 *
 * Write the 32-bit value specified in val to the register
 * address specified in addr.
 */
void osa_reg32_write (uint32 val, uint_addr addr);

/**
 * osa_reg32_read - Read the value from a 32-bit register
 *
 * @addr: Address of the register to read
 *
 * Read the value from the register specified by addr.
 * Returns the 32-bit value read from the register.
 */
uint32 osa_reg32_read (uint_addr addr);

/**
 * osa_reg32_poll_iseq - Poll for a register to have a certain value
 *
 * @addr: Address of the register to poll
 * @req_val: Required value of the register
 * @mask: Mask to be used on the result of the register read before checking
 * @cnt: Number of times to poll
 *
 * Poll the register cnt times or until the masked value read from the register
 * is equal to req_val.
 * Returns 0 upon success.
 * Returns -1 upon timeout.
 */
int32 osa_reg32_poll_iseq(uint_addr addr, uint32 req_val, uint32 mask, uint32 cnt);

/* return the base address of decoder register */
void *osa_get_decoder_base_reg_addr(void);

void *osa_get_encoder_base_reg_addr(void);


#endif /* OSA_REG_IO_H_ */
