/* SPDX-License-Identifier: GPL-2.0 */
/*
 * osa definitions header
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef OSA_SEMAPHORE_H_
#define OSA_SEMAPHORE_H_

#include "osa_types.h"

/*!
 *  @brief    Mode of the semaphore
 */
enum osa_semaphore_mode {
    osa_semaphore_mode_counting = 0x0,
    osa_semaphore_mode_binary   = 0x1,
	osa_semaphore_mode_FORCE32BITS = 0x7FFFFFFFU
};

enum osa_semaphore_wait {
	osa_semaphore_no_wait,
	osa_semaphore_wait_forever = -1,
	osa_semaphore_wait_FORCE32BITS = 0x7FFFFFFFU
};

/*!
 *  @brief  Function to create a semaphore.
 *
 *  @param  mode   This should be binary semaphore or counting semaphore
 *  @param  count  Initial count of the semaphore. For binary semaphores,
 *                 only values of 0 or 1 are valid.
 *
 *  @return A Semaphore Handle on success or a NULL on an error
 */
void *osa_semaphore_create(enum osa_semaphore_mode mode, uint32 count);

/*!
 *  @brief  Function to delete a semaphore.
 *
 *  @param  handle returned from ::osa_semaphore_create
 *
 *  @return Status of the functions
 *    -0 on success.
 *    -ve value on failure.
 */
int32 osa_semaphore_delete(void *handle);

/*!
 *  @brief  Function to pend (wait) on a semaphore.
 *
 *  @param  handle returned from ::osa_semaphore_create
 *
 *  @param  timeout Timeout (in milliseconds) to wait for the semaphore to
 *                  be posted (signalled).
 *
 *  @return Status of the functions
 *    -0 on success.
 *    -ve value on failure.
 */
int32 osa_semaphore_pend(void *handle, uint32 timeout);

/*!
 *  @brief  Function to post (signal) a semaphore.
 *
 *  @param  handle returned from ::osa_semaphore_create
 *
 *  @return Status of the functions
 *    -0 on success.
 *    -ve value on failure.
 */
int32 osa_semaphore_post(void *handle);

#endif /* OSA_SEMAPHORE_H_ */
