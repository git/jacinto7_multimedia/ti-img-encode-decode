/* SPDX-License-Identifier: GPL-2.0 */
/*
 * osa platform specific handling definitions
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef OSA_PLATFORM_H_
#define OSA_PLATFORM_H_

void *osa_platform_get_drvdata(void *dev);

void *osa_dev_get_drvdata(void *dev);

#endif /* OSA_PLATFORM_H_ */
