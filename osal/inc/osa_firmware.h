/* SPDX-License-Identifier: GPL-2.0 */
/*
 * osa firmware related definitions
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef OSA_FIRMWARE_H_
#define OSA_FIRMWARE_H_

#include "osa_types.h"

/* Fetch firmware from the file system */
void osa_init_completion(void **firmware_loading_complete_args);

/**
 * osa_request_firmware_nowait() - asynchronous version of request_firmware
 * @module: module requesting the firmware
 * @name: name of firmware file
 * @device: device for which firmware is being loaded
 * @context: will be passed over to @cont, and
 *  @fw may be %NULL if firmware request fails.
 * @cont: function will be called asynchronously when the firmware
 *  request is over.
 *
 *  Caller must hold the reference count of @device
 **/
int32 osa_request_firmware_nowait(void *module, const uint8 *name,
        void *device, void *context, void *fp);

/**
 * osa_complete_all: - signals all threads waiting on this completion
 * @x:  holds the state of this particular completion
 *
 * This will wake up all threads waiting on this particular completion event.
 *
 * If this function wakes up a task, it executes a full memory barrier before
 * accessing the task state.
 *
 * Since osa_complete_all() sets the completion of @x permanently to done
 * to allow multiple waiters to finish.
 */
void osa_complete_all(void *x);

/**
 * osa_wait_for_completion: - waits for completion of a task
 * @x:  holds the state of this particular completion
 *
 * This waits to be signaled for completion of a specific task. It is NOT
 * interruptible and there is no timeout.
 */
void osa_wait_for_completion(void *x);

/**
 * osa_release_firmware() - release the resource associated with a firmware image
 * @fw: firmware resource to release
 **/
void osa_release_firmware(void *fw);

/* Return size of firmware data */
size_t osa_get_fw_size(void *fw_args);

/* Return the firmware data pointer */
const uint8 *osa_get_fw_data(void *fw_args);

#endif /* OSA_FIRMWARE_H_ */
