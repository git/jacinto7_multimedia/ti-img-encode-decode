/* SPDX-License-Identifier: GPL-2.0 */
/*
 * osa event definitions
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2019 Texas Instruments Incorporated - http://www.ti.com/
 */

#ifndef OSA_EVENT_H_
#define OSA_EVENT_H_

#include "osa_types.h"

/**
 * osa_create_event_obj - allocate and init the event
 *
 * @event: Pointer to event object
 *
 * Allocate and initialize the event object.
 * Returns error if fails to allocate.
 */
int32 osa_create_event_obj(void ** event);


/**
 * osa_destroy_event_obj - destroy and free the event
 * @event: event object to destroy
 *
 * This function frees the event object.
 */
void osa_destroy_event_obj(void * event);


/**
 * osa_signal_event_obj - signals the event object
 * @event: event object
 *
 * Signals the event object specified.
 */
void osa_signal_event_obj(void *event);


/**
 * osa_wait_event_obj - wait on the event object
 * @event: event object
 * @uninterruptible: flag to indicate if wait should be interruptible
 * @timeout: timeout to wait for event
 *
 * Waits for the event. If uninterruptible is set to false, then the
 * call is interruptible.
 * Returns IMG_SUCCESS when event is signalled.
 * Returns IMG_ERROR_INTERRUPTED if it is interrupted.
 */
int32 osa_wait_event_obj(void *event, osa_bool uninterruptible, uint32 timeout);

#endif /* OSA_EVENT_H_ */
