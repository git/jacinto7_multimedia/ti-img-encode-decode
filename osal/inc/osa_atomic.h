/* SPDX-License-Identifier: GPL-2.0 */
/*
 * osa atomic definitions
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2019 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef OSA_ATOMIC_H_
#define OSA_ATOMIC_H_

#include "osa_types.h"

/**
 * osa_atomic_create - allocate and initializes the mutex
 *
 * @atomic_args: Atomic variable
 *
 * Allocate and initialize the atomic variable.
 * Returns error if fails to allocate.
 */
int32 osa_atomic_create (void **atomic_args);


/**
 * osa_atomic_destroy - destroy and release the atomic object
 * @atomic_args: Atomic variable
 *
 * This function marks the atomic variable uninitialized, and any subsequent
 * use is forbidden.
 */
void osa_atomic_destroy(void **atomic_args);


/**
 * osa_atomic_read - read the value of the atomic variable
 * @atomic_args: Atomic variable
 *
 * Returns the value of the atomic variable. Returns -1 if fails.
 */
int32 osa_atomic_read(void *atomic_args);


/**
 * osa_atomic_set - set the value of the atomic variable
 * @atomic_args: Atomic variable
 *
 * Sets the value of the atomic variable.
 */
void osa_atomic_set(void *atomic_args, int32 val);


/**
 * osa_atomic_inc_ret - Increment the atomic var and return the value.
 * @atomic_args: Atomic variable
 *
 * Returns the value of the atomic variable after adding 1.
 * Returns -1 if fails.
 */
int32 osa_atomic_inc_ret(void *atomic_args);


/**
 * osa_atomic_dec_ret - Decrement the atomic var and return the value.
 * @atomic_args: Atomic variable
 *
 * Returns the value of the atomic variable after subtracting 1.
 * Returns -1 if fails.
 */
int32 osa_atomic_dec_ret(void *atomic_args);

#endif /* OSA_ATOMIC_H_ */
