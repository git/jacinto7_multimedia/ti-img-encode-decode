/* SPDX-License-Identifier: GPL-2.0 */
/*
 * osa spinlock definitions
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef OSA_SPIN_H_
#define OSA_SPIN_H_

#include "osa_types.h"

void *appMemMap_2(void *phys_ptr, uint32_t size);

 /**
  * osa_spin_lock_create -  Allocate memory and initializing spin-lock to unlocked state
  *
  * @lock: spin-lock variable
  * @name: Pointer to a name for the new spinlock. Only the pointer,
  * but not the whole name, is copied into the spinlock.
  * The name is used for diagnostics purposes only.
  * This argument used in RTOS platform
  */
void osa_spin_lock_create(void **lock_args, const int8 *name);

/**
 * osa_spin_lock -  This will take the lock if it is free,
 * otherwise it’ll spin until that lock is free (Keep trying).
 *
 * @lock: Spin-lock variable
 */
void osa_spin_lock(void *lock);

/**
 * osa_spin_trylock -  Locks the spinlock if it is not already locked.
 * If unable to obtain the lock it exits with an error and do not spin.
 *
 * @lock: spin-lock variable
 *
 * It returns non-zero if obtains the lock otherwise returns zero.
 */
int32 osa_spin_trylock(void *lock);

/**
 * osa_spin_unlock -  It does the reverse of lock.
 *
 * @lock: spin-lock variable
 */
void osa_spin_unlock(void *lock);

/**
 * osa_spin_is_locked -  This is used to check whether the lock is available or not.
 *
 * @lock: spin-lock variable
 *
 * It returns non-zero if the lock is currently acquired. otherwise returns zero.
 */
int32 osa_spin_is_locked(void *lock);

/**
 * osa_spin_lock_irqsave - This will save whether interrupts
 * were on or off in a flags word and grab the lock.
 *
 * @lock: spin-lock variable
 * @flags: flag to save the interrupt status
 */
void osa_spin_lock_irqsave(void *lock, ulong *flags);

/**
 * osa_spin_unlock_irqrestore - This will releases the spinlock and restores
 * the interrupts using the flags argument.
 *
 * @lock: spin-lock variable
 * @flags: flag contains interrupt status
 */
void osa_spin_unlock_irqrestore(void *lock, uint64 flags);

/**
 * osa_spin_lock_irq - This will disable interrupts on that cpu,
 * then grab the lock.
 *
 * @lock: spin-lock variable
 */
void osa_spin_lock_irq(void *lock);

/**
 * osa_spin_unlock_irq -  It will release the lock and re-enables
 * the interrupts which is disabled by above call.
 *
 * @lock: spin-lock variable
 */
void osa_spin_unlock_irq(void *lock);

/**
 * osa_spin_destroy - destroy and release the spin object
 * @spin_args: spin variable
 *
 * This function marks the spin uninitialized, and any subsequent
 * use of the spin is forbidden.
 */
void osa_spin_destroy(void **lock_args);

#endif /* OSA_SPIN_H_ */
