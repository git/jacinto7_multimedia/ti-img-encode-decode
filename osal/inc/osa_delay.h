/* SPDX-License-Identifier: GPL-2.0 */
/*
 * osa delay functions definitions
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef OSA_DELAY_H_
#define OSA_DELAY_H_

#include "osa_types.h"

/**
 * osa_udelay - sleep safely even with waitqueue interruptions
 * @usecs: Time in microseconds to sleep for
 */
void osa_udelay(uint64 usecs);
/**
 * osa_msleep - sleep safely even with waitqueue interruptions
 * @msecs: Time in milliseconds to sleep for
 */
void osa_msleep(uint32 msecs);

/**
 * osa_usleep_range - Sleep for an approximate time
 * @min: Minimum time in usecs to sleep
 * @max: Maximum time in usecs to sleep
 */
void osa_usleep_range(uint64 min, uint64 max);

/**
 * osa_ndelay - sleep safely even with waitqueue interruptions
 * @nsecs: Time in nanoseconds to sleep for
 */
void osa_ndelay(uint64 nsecs);

#endif /* OSA_DELAY_H_ */
