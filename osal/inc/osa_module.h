/* SPDX-License-Identifier: GPL-2.0 */
/*
 * osa definitions header
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef OSA_MODULE_H_
#define OSA_MODULE_H_

#include "osa_types.h"

/* power-on decoder module
 *
 * Return 0 on success*/
int32 osa_pm_set_module_state_decoder_on();

/* power-off decoder module
 *
 * Return 0 on success*/
int32 osa_pm_set_module_state_decoder_off();

/* power-on encoder module
 *
 * Return 0 on success*/
int32 osa_pm_set_module_state_encoder_on();

/* power-off encoder module
 *
 * Return 0 on success*/
int32 osa_pm_set_module_state_encoder_off();

#endif /* OSA_MODULE_H_ */
