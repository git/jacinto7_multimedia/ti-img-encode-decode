/* SPDX-License-Identifier: GPL-2.0 */
/*
 * osa definitions header
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef OSA_HWI_H_
#define OSA_HWI_H_

#include "osa_types.h"

/*!
 *  @brief  Function to create an interrupt on CortexM devices
 *
 *  @param  hwiFxn entry function of the hardware interrupt
 *
 *  @param  arg argument passed to the hwiFxn
 *
 *  @return A Hwi_Handle on success or a NULL
 */
void *osa_hwi_create(void *hwi_fxn, void *arg, uint8_t hwa_id);

/*!
 *  @brief  Function to delete an interrupt on devices
 *
 *  @param  handle returned from the osa_hwi_create call
 *
 *  @return 0 on success else -ve value returned
 */
int32 osa_hwi_delete(void *handle);

#endif /* OSA_HWI_H_ */
