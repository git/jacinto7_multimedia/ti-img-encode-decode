// SPDX-License-Identifier: GPL-2.0
/*
 * osa memory handling functions implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <string.h>

#include "../../inc/osa_define.h"
#include "../../inc/osa_mem.h"
#include "mm_common.h"

#ifdef DEBUG_OSA_MEM
#include "../../inc/osa_mutex.h"

void *lock = NULL;
#define MAX_NUM_RESOURCES 5000
static struct memory_release resource[MAX_NUM_RESOURCES] = {0};
static uint32 count = 0;
static uint32 init = 0;

void osa_save_mem(void *ptr, uint32 size, const uint8 *func, uint32 line)
{
    uint32 i = 0;

    if(0 == init)
    {
        init = 1;
        osa_mutex_create(&lock);
    }

    osa_mutex_lock(lock);
    for (i = 0; i < MAX_NUM_RESOURCES; i++) {
        if (0 == resource[i].flag) {
            resource[i].flag = 1;
            strcpy(resource[i].func, func);
            resource[i].line = line;
            resource[i].ptr = ptr;
            resource[i].size = size;
            count++;
            osa_mutex_unlock(lock);
            return;
        }
    }
    osa_mutex_unlock(lock);
    OSA_PR_ERR("##### NO MEMORY #######\n");
}

void osa_remove_mem(void *ptr, const uint8 *func, uint32 line)
{
    uint32 i = 0;

    osa_mutex_lock(lock);
    for (i = 0; i < MAX_NUM_RESOURCES; i++) {
        if (1 == resource[i].flag) {
            if (ptr == resource[i].ptr) {
                resource[i].flag = 0;
                count--;
                osa_mutex_unlock(lock);
                return;
            }
        }
    }
    osa_mutex_unlock(lock);
    OSA_PR_ERR("##### INVALID ADDRESS ####### [0x%llx] function_name = %s line_no = %d\n",
               (uint64)ptr, func, line);
}

void osa_print_mem_leak()
{
    uint32 i = 0;

    OSA_PR_ERR("##### printing %d outstanding allocations #####\n", count);
    for (i = 0; i < MAX_NUM_RESOURCES; i++) {
        if (1 == resource[i].flag) {
            OSA_PR_ERR("function_name = %s, line_no = %d, ptr = 0x%llx, size = %d\n",
                       resource[i].func, resource[i].line, (uint64)resource[i].ptr,
                       resource[i].size);
        }
    }
    osa_mutex_destroy(&lock);
}
#endif


#define OFFSET (4*1024)

#ifdef DEBUG_OSA_MEM
void *osa_calloc_debug(uint64 n, uint64 size, int64 flags, const uint8 *func, uint32 line)
#else
void *osa_calloc(uint64 n, uint64 size, int64 flags)
#endif
{
    void *ptr = NULL;
#ifndef DEBUG_OSA_MEM
    ptr = osa_malloc((n*size), flags);
#else
    ptr = osa_malloc_debug((n*size), flags, func, line);
#endif
    memset(ptr, 0, n*size);

    return ptr;
}

#ifdef DEBUG_OSA_MEM
void *osa_malloc_debug(uint64 size, int64 flags, const uint8 *func, uint32 line)
#else
void *osa_malloc(uint64 size, int64 flags)
#endif
{
    void *ptr = NULL;
    uint32 *ptr1;

    size  = ALIGN32(size, OFFSET) + OFFSET;
    ptr = g_MM_COMMON_Inst.osa_params.memAlloc((uint32)size);
    ptr1 = (uint32*)ptr;
    *ptr1 = (uint32)size;
    ptr = (void*)((uint32)ptr + OFFSET);
#ifdef DEBUG_OSA_MEM
    osa_save_mem(ptr, size, func, line);
#endif

    return ptr;
}

void *osa_realloc(const void *p, uint64 new_size, int64 flags)
{
    osa_free(p);
    return osa_malloc(new_size, flags);
}

#ifdef DEBUG_OSA_MEM
void osa_free_debug(const void *objp, const uint8 *func, uint32 line)
#else
void osa_free(const void *objp)
#endif
{
    uint32 size;
    uint32 *ptr;

    if(NULL != objp)
    {
#ifdef DEBUG_OSA_MEM
        osa_remove_mem((void *)objp, func, line);
#endif
        objp = (void*)((uint32)objp - OFFSET);
        ptr = (uint32*)objp;
        size = *ptr;
        g_MM_COMMON_Inst.osa_params.memFree((void*)objp, size);
    }
}

#ifdef DEBUG_OSA_MEM
void *osa_zalloc_debug(uint64 size, int64 flags, const uint8 *func, uint32 line)
#else
void *osa_zalloc(uint64 size, int64 flags)
#endif
{
    void *ptr = NULL;

#ifndef DEBUG_OSA_MEM
    ptr = osa_malloc(size, flags);
#else
    ptr = osa_malloc_debug(size, flags, func, line);
#endif
    memset(ptr, 0, size);

    return ptr;
}

#ifdef DEBUG_OSA_MEM
void *osa_malloc_array_debug(size_t n, size_t size, int64 flags, const uint8 *func, uint32 line)
#else
void *osa_malloc_array(size_t n, size_t size, int64 flags)
#endif
{
	void *ptr = NULL;

#ifndef DEBUG_OSA_MEM
	ptr = osa_malloc((uint64)(n*size), flags);
#else
	ptr = osa_malloc_debug((uint64)(n*size), flags, func, line);
#endif

    return ptr;
}

#ifdef DEBUG_OSA_MEM
int8 *osa_strdup_debug(const int8 *s, int64 flags, const uint8 *func, uint32 line)
#else
int8 *osa_strdup(const int8 *s, int64 flags)
#endif
{

    int i = 0;
    void *ptr = NULL;

    while(1)
    {
		if (s[i] == '\0')
		{
			break;
		}
		i++;
	}
#ifndef DEBUG_OSA_MEM
    ptr = osa_malloc((uint64)(i+1), flags);
#else
    ptr = osa_malloc_debug((uint64)(i+1), flags, func, line);
#endif
    memcpy(ptr, s, i);

    return ptr;
}

#ifdef DEBUG_OSA_MEM
void *osa_memdup_debug(const void *src, size_t len, int64 flags, const uint8 *func, uint32 line)
#else
void *osa_memdup(const void *src, size_t len, int64 flags)
#endif
{
    void *ptr = NULL;

#ifndef DEBUG_OSA_MEM
    ptr = osa_malloc((uint64)len, flags);
#else
    ptr = osa_malloc_debug((uint64)len, flags, func, line);
#endif
    memcpy(ptr, src, len);

    return ptr;
}

void *osa_vmap(void **pages, uint32 count)
{
    struct page *page = (struct page *)*pages;

    return (page->sgt->sgl->dma_address);
}

void osa_vunmap(const void *addr)
{
	/* Nothing to do here for TIRTOS */
    return;
}
