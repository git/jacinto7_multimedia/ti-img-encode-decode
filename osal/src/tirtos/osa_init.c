// SPDX-License-Identifier: GPL-2.0
/*
 * osa time handling functions implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <stdarg.h>

#include "../../inc/osa_types.h"
#include "../../inc/osa_define.h"
#include "mm_common.h"

void MM_COMMON_OsaInit(struct mm_osa_cfg *params)
{
	g_MM_COMMON_Inst.osa_params.memAlloc = params->memAlloc;
	g_MM_COMMON_Inst.osa_params.memFree = params->memFree;
	g_MM_COMMON_Inst.osa_params.printFxn = params->printFxn;
}

void VideoCodec_printf(const char *format, ...)
{
    va_list     vaArgPtr;
    char       *buf;

    if(g_MM_COMMON_Inst.osa_params.printFxn != NULL)
    {
        buf = &g_MM_COMMON_Inst.printBuf[0];
        (void) va_start(vaArgPtr, format);
        (void) vsnprintf(
            buf, CODEC_CFG_PRINT_BUF_LEN, (const char *) format, vaArgPtr);
        va_end(vaArgPtr);

        g_MM_COMMON_Inst.osa_params.printFxn(buf);
    }

    return;
}
