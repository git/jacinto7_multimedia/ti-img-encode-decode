// SPDX-License-Identifier: GPL-2.0
/*
 * osa time handling functions implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <ti/csl/soc.h>
#include "osa_reg_io.h"
#include "osa_define.h"
#include "osa_delay.h"

void *osa_get_decoder_base_reg_addr(void)
{
	return (void *)CSL_DECODER0_MTX_CORE_BASE;
}

void *osa_get_encoder_base_reg_addr(void)
{
	return (void *)CSL_ENCODER0_REG_TOPAZHP_MULTICORE_BASE; // or CSL_ENCODER0_REG_MTX_BASE??
}


void osa_reg32_write(uint32 val, uint_addr addr)
{
	HW_WR_REG32_RAW((uint32_t)addr, val);
}

uint32 osa_reg32_read(uint_addr addr)
{
	return HW_RD_REG32_RAW((uint32_t)addr);
}

int32 osa_reg32_poll_iseq(uint_addr addr, uint32 req_val, uint32 mask, uint32 cnt)
{
	uint32 count, val;
	int32 res = 0;

	/* Add high-frequency poll loops. */
	cnt += 10;

	/*
	 * High-frequency loop (designed for shorter hardware latency such as
	 * reset).
	 */
	for (count = 0; count < cnt; count++) {
		/* Read from the device */
		val = osa_reg32_read(addr);
		val = (val & mask);

		if (val == req_val) {
			res = 0;
			break;
		}

		/*
		 * Sleep to wait for hardware.
		 * Period is selected to allow for high-frequency polling
		 * (5us, e.g. reset) over the first 10 iterations, then
		 * reverting to a lower-frequency (100us, e.g. DMA) for the
		 * remainder.
		 */
		if (count < 10)
			osa_usleep_range(5, 5);
		else
			osa_usleep_range(100, 100);
	}

	if (res || (count >= cnt)) {
		OSA_PR_INFO("Poll failed!\n");
		res = -1;
	}

	return res;
}
