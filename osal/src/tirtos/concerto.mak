ifeq ($(TARGET_PLATFORM),J7)
ifeq ($(TARGET_OS),SYSBIOS)

include $(PRELUDE)
TARGET      := video_codec_osal_tirtos
TARGETTYPE  := library

IDIRS       += $(VIDEO_CODEC_PATH)/examples
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/driver/common
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/driver/decoder
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/osal/inc
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/timmlib/include
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/timmlib/decoder
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/driver/encoder
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/timmlib/encoder
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/timmlib/include

include $($(_MODULE)_SDIR)/../../../tirtos/concerto_config_inc.mak

OSAL_FILES_PATH   =

CSOURCES    := \
			$(OSAL_FILES_PATH)/osa_init.c \
			$(OSAL_FILES_PATH)/osa_mem.c \
			$(OSAL_FILES_PATH)/osa_delay.c \
			$(OSAL_FILES_PATH)/osa_mutex.c \
			$(OSAL_FILES_PATH)/osa_time.c \
			$(OSAL_FILES_PATH)/osa_idr.c \
			$(OSAL_FILES_PATH)/osa_platform.c \
			$(OSAL_FILES_PATH)/osa_spin.c \
			$(OSAL_FILES_PATH)/osa_firmware.c \
			$(OSAL_FILES_PATH)/osa_interrupt.c \
			$(OSAL_FILES_PATH)/osa_page.c \
			$(OSAL_FILES_PATH)/osa_workqueue.c \
			$(OSAL_FILES_PATH)/osa_semaphore.c \
			$(OSAL_FILES_PATH)/osa_queue.c \
			$(OSAL_FILES_PATH)/osa_hwi.c \
			$(OSAL_FILES_PATH)/osa_module.c \
			$(OSAL_FILES_PATH)/osa_reg_io.c \
			$(OSAL_FILES_PATH)/osa_task.c \
			$(OSAL_FILES_PATH)/osa_string.c \
			$(OSAL_FILES_PATH)/osa_event.c \

include $(FINALE)

endif
endif
