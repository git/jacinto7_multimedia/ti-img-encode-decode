// SPDX-License-Identifier: GPL-2.0
/*
 * osa firware request functions implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "../../inc/osa_firmware.h"
#include "osa_firmware_data.h"
#include <osa_mem.h>

struct firmware {
	size_t size;
	uint8 *data;
};

void osa_init_completion(void **firmware_loading_complete_args)
{
	uint8 size = 1;
	/* Dummy implementation for TI RTOS */
	*firmware_loading_complete_args = osa_malloc(size, 0);
}

int32 osa_request_firmware_nowait(void *module, const uint8 *name,
        void *device, void *context, void *fp)
{
	struct firmware fw;
	void (*cont)(const struct firmware *fw, void *context);

	fw.size = sizeof(firmware_data);
	fw.data = firmware_data;

	cont = (void (*)(const struct firmware *, void *))fp;
	cont(&fw, context);

	return 0;
}

void osa_complete_all(void *x)
{
	/* Dummy implementation for TI RTOS */
}

void osa_wait_for_completion(void *x)
{
	/* Dummy implementation for TI RTOS */
}

void osa_release_firmware(void *fw)
{
	/* Dummy implementation for TI RTOS */
}

size_t osa_get_fw_size(void *fw_args)
{
    struct firmware *fw = (struct firmware *)fw_args;
    return fw->size;
}

const uint8 *osa_get_fw_data(void *fw_args)
{
    struct firmware *fw = (struct firmware *)fw_args;
    return fw->data;
}
