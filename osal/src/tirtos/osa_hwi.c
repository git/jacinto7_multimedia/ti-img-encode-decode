// SPDX-License-Identifier: GPL-2.0
/*
 * osa time handling functions implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "osa_hwi.h"
#include <ti/osal/HwiP.h>
#include <ti/csl/soc.h>

#if defined(R5F)
#define DECODER0_IRQ  (CSLR_R5FSS0_CORE0_INTR_DECODER0_IRQ_0)
#else
#define DECODER0_IRQ  (CSLR_ARMSS0_CPU0_INTR_GPIOMUX_INTRTR0_OUTP_20)
#endif

#if defined(R5F)
#define ENCODER0_IRQ  (CSLR_R5FSS0_CORE0_INTR_ENCODER0_IRQ_0)
#else
#define ENCODER0_IRQ  (CSLR_ARMSS0_CPU0_INTR_GPIOMUX_INTRTR0_OUTP_20)
#endif

void *osa_hwi_create(void *hwi_fxn, void *arg, uint8_t hwa_id)
{
	HwiP_Handle intr_handle;
	HwiP_Params hwiParams;
	uint32_t interrupt_id = 0;

	switch (hwa_id) {
		case HWA_DECODER:
			interrupt_id = DECODER0_IRQ;
			break;
		case HWA_ENCODER:
			interrupt_id = ENCODER0_IRQ;
			break;
		default:
			return NULL;
	}
			
	/* Clear out any pending interrupts */
	HwiP_clearInterrupt(interrupt_id);

	/* Populate the interrupt parameters */
	HwiP_Params_init(&hwiParams);
	hwiParams.arg = (uintptr_t)arg;

	/* Register interrupt */
	intr_handle = HwiP_create(interrupt_id,
			(HwiP_Fxn)hwi_fxn, &hwiParams);

	return intr_handle;
}

int32 osa_hwi_delete(void *handle)
{
	return HwiP_delete(handle);
}
