// SPDX-License-Identifier: GPL-2.0
/*
 * osa time handling functions implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <ti/drv/sciclient/sciclient.h>
#include <ti/csl/csl_types.h>
#include "osa_module.h"


int32 osa_pm_set_module_state_decoder_on()
{
	return Sciclient_pmSetModuleState(TISCI_DEV_DECODER0,
			TISCI_MSG_VALUE_DEVICE_SW_STATE_ON,
			TISCI_MSG_FLAG_AOP,
			SCICLIENT_SERVICE_WAIT_FOREVER);
}

int32 osa_pm_set_module_state_decoder_off()
{
	return Sciclient_pmSetModuleState(TISCI_DEV_DECODER0,
			TISCI_MSG_VALUE_DEVICE_HW_STATE_OFF,
			TISCI_MSG_FLAG_AOP,
			SCICLIENT_SERVICE_NO_WAIT);
}

int32 osa_pm_set_module_state_encoder_on()
{
	return Sciclient_pmSetModuleState(TISCI_DEV_ENCODER0,
			TISCI_MSG_VALUE_DEVICE_SW_STATE_ON,
			TISCI_MSG_FLAG_AOP,
			SCICLIENT_SERVICE_WAIT_FOREVER);
}

int32 osa_pm_set_module_state_encoder_off()
{
	return Sciclient_pmSetModuleState(TISCI_DEV_ENCODER0,
			TISCI_MSG_VALUE_DEVICE_HW_STATE_OFF,
			TISCI_MSG_FLAG_AOP,
			SCICLIENT_SERVICE_NO_WAIT);
}
