// SPDX-License-Identifier: GPL-2.0
/*
 * osa time handling functions implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <string.h>

#include <xdc/runtime/Types.h>
#include <xdc/runtime/Timestamp.h>
#include "../../inc/osa_define.h"
#include "../../inc/osa_time.h"
#include "../../inc/osa_mem.h"


uint64 osa_get_get_time_in_usec()
{
    Types_Timestamp64 bios_timestamp64;
    Types_FreqHz bios_freq;
    uint64_t cur_ts, freq;

    Timestamp_get64(&bios_timestamp64);
    Timestamp_getFreq(&bios_freq);

    cur_ts = ((uint64_t) bios_timestamp64.hi << 32) | bios_timestamp64.lo;
    freq = ((uint64_t) bios_freq.hi << 32) | bios_freq.lo;

    return (cur_ts*1000000u)/freq;
}

uint64 osa_get_number_of_ticks(void)
{
    Types_Timestamp64 bios_timestamp64;
    uint64_t cur_ts;

    Timestamp_get64(&bios_timestamp64);
    cur_ts = ((uint64_t) bios_timestamp64.hi << 32) | bios_timestamp64.lo;

    return cur_ts;
}

uint64 osa_msecs_to_jiffies(uint32 m)
{
    Types_FreqHz bios_freq;
    uint64_t freq;

    Timestamp_getFreq(&bios_freq);
    freq = ((uint64_t) bios_freq.hi << 32) | bios_freq.lo;

    return (m*freq)/1000u;
}

uint64 osa_jiffies_to_msecs(uint64 j)
{
    Types_FreqHz bios_freq;
    uint64_t freq;

    Timestamp_getFreq(&bios_freq);
    freq = ((uint64_t) bios_freq.hi << 32) | bios_freq.lo;

    return (j*1000u)/freq;
}

void osa_getnstimeofday(struct osa_timespec *ts_args)
{
    uint64 ts64;

    if(NULL == ts_args)
    {
        OSA_PR_ERR("Invalid ts_args pointer in 'osa_getnstimeofday' function \n");
    }
    else
    {
        ts64 = osa_get_get_time_in_usec();
        memcpy(&ts_args->ts, &ts64, sizeof(uint64));
    }
}

void osa_timespec_sub(struct osa_timespec *end_time, struct osa_timespec *start_time, struct osa_timespec *dif_time)
{

	if(end_time->ts >= start_time->ts) {
		dif_time->ts = end_time->ts - start_time->ts;
	}else {
		dif_time->ts = end_time->ts + (0xFFFFFFFFFFFFFFFFU - start_time->ts);
	}
}

/**
 * osa_timespec_to_ns - Convert microseconds to nanoseconds
 * @ts_args:     pointer to the variable to be converted
 *
 * Returns the given time in nanosecond
 */
int64 osa_timespec_to_ns(struct osa_timespec *ts_args)
{
    return ts_args->ts * 1000u;
}
