// SPDX-License-Identifier: GPL-2.0
/*
 * osa time handling functions implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <ti/osal/HwiP.h>
#include <ti/osal/SemaphoreP.h>
#include "../../inc/osa_semaphore.h"
#include "../../inc/osa_queue.h"
#include "../../inc/osa_define.h"

#define QUEUE_ASSERT(x)  { if((x) == 0){ \
			OSA_PR_ERR(" Assertion @ Line: %d in %s: failed !!!\n",\
				__LINE__, __FILE__);\
				while(1); }}

void osa_utils_quecreate(osa_utils_quehandle *handle,
                      uint32 maxElements, void *queueMem, uint32 flags)
{
	SemaphoreP_Params semParams;
    /*
     * init handle to 0's
     */
    memset(handle, 0, sizeof(osa_utils_quehandle));

    /*
     * init handle with user parameters
     */
    handle->maxElements = maxElements;
    handle->flags = flags;

    /*
     * queue data element memory cannot be NULL
     */
    QUEUE_ASSERT(queueMem != NULL);

    handle->queue = queueMem;

	SemaphoreP_Params_init(&semParams);
	semParams.mode = SemaphoreP_Mode_BINARY;

    if (handle->flags & OSA_UTILS_QUE_FLAG_BLOCK_QUE_GET)
    {
        /*
         * user requested block on que get
         */

        /*
         * create semaphore for it
         */
        handle->semRd = SemaphoreP_create((int32)0, &semParams);
        QUEUE_ASSERT(handle->semRd != NULL);
    }

    if (handle->flags & OSA_UTILS_QUE_FLAG_BLOCK_QUE_PUT)
    {
        /*
         * user requested block on que put
         */

        /*
         * create semaphore for it
         */
        handle->semWr = SemaphoreP_create((int32)0, &semParams);

        QUEUE_ASSERT(handle->semWr != NULL);
    }
    handle->blockedOnGet = (osa_bool)FALSE;
    handle->blockedOnPut = (osa_bool)FALSE;
    handle->forceUnblockGet = (osa_bool)FALSE;
    handle->forceUnblockPut = (osa_bool)FALSE;

    return;
}

int32 osa_utils_quedelete(osa_utils_quehandle *handle)
{
    if (handle->flags & OSA_UTILS_QUE_FLAG_BLOCK_QUE_GET)
    {
        /*
         * user requested block on que get
         */

        /*
         * delete associated semaphore
         */

        SemaphoreP_delete(handle->semRd);

    }
    if (handle->flags & OSA_UTILS_QUE_FLAG_BLOCK_QUE_PUT)
    {
        /*
         * user requested block on que put
         */

        /*
         * delete associated semaphore
         */

        SemaphoreP_delete(handle->semWr);
    }

    return OSA_STATUS_SOK;
}

int32 osa_utils_queput(osa_utils_quehandle *handle, void *data, uint32 timeout)
{
	int32 status = OSA_STATUS_EFAIL;/* init status to error */
	uint32 cookie;
	volatile osa_bool doBreak = FALSE;

	if ((uint32) osa_semaphore_wait_forever == timeout) {
		timeout = SemaphoreP_WAIT_FOREVER;
	}
	else if ((uint32) osa_semaphore_no_wait == timeout) {
		timeout = SemaphoreP_NO_WAIT;
	}

    do
    {
        /*
         * disable interrupts
         */
        cookie = HwiP_disable();

        if (handle->count < handle->maxElements)
        {
            /*
             * free space available in que
             */

            /* MISRA.PTR.ARITH
             * MISRAC_2004_Rule_11.1
             * MISRAC_WAIVER:
             * Pointer is accessed as an array.
             * Queue user always allocates queue of size maxElements, queue
             * will not be accessed out of bound.
             */
            /*
             * insert element
             */
            handle->queue[handle->curWr] = data;

            /*
             * increment put pointer
             */
            handle->curWr = (handle->curWr + 1) % handle->maxElements;

            /*
             * increment count of number element in que
             */
            handle->count++;

            /*
             * restore interrupts
             */
            HwiP_restore(cookie);

            /*
             * mark status as success
             */
            status = OSA_STATUS_SOK;

            if (handle->flags & OSA_UTILS_QUE_FLAG_BLOCK_QUE_GET)
            {
                /*
                 * blocking on que get enabled
                 */

                /*
                 * post semaphore to unblock, blocked tasks
                 */
                SemaphoreP_post(handle->semRd);
            }

            /*
             * exit, with success
             */
            doBreak = (osa_bool)TRUE;

        }
        else
        {
            /*
             * que is full
             */

            /*
             * restore interrupts
             */
            HwiP_restore(cookie);

            if (timeout == SemaphoreP_NO_WAIT)
            {
                doBreak = (osa_bool)TRUE;                    /* non-blocking
                                                            * function call,
                                                            * exit with error
                                                            */
            }
            else if (handle->flags & OSA_UTILS_QUE_FLAG_BLOCK_QUE_PUT)
            {
                osa_bool semPendStatus;

                /*
                 * blocking on que put enabled
                 */

                /*
                 * take semaphore and block until timeout occurs or
                 * semaphore is posted
                 */
                handle->blockedOnPut = (osa_bool)TRUE;
                semPendStatus = SemaphoreP_pend(handle->semWr, timeout);
                handle->blockedOnPut = (osa_bool)FALSE;
                if (!semPendStatus)
                {
                    /* UNREACH.GEN :  MISRAC_2004_Rule_14.1
                     * Unreachable Code
                     * MISRAC_WAIVER:
                     * Value in the if condition is dependent on the
                     * return of a function SemaphoreP_pend and
                     * this function is implemented when OS BIOS is
                     * included/defined.
                     */
                    handle->forceUnblockPut = (osa_bool)FALSE;
                    doBreak = (osa_bool)TRUE;                /* timeout
                                                            * happend, exit
                                                            * with error */
                }
                else if (handle->forceUnblockPut)
                {
                    handle->forceUnblockPut = (osa_bool)FALSE;
                    doBreak = (osa_bool)TRUE;                /* timeout
                                                            * happend, exit
                                                            * with error */
                }
                else
                {
                    doBreak = (osa_bool)FALSE;
                }
                /*
                 * received semaphore, recheck for available space in the que
                 */
            }
            else
            {
                /*
                 * blocking on que put disabled
                 */

                /*
                 * exit with error
                 */
                doBreak = (osa_bool)TRUE;
            }
        }

        if ((osa_bool)TRUE == doBreak)
        {
            break;
        }
    }
    while (1);

    return status;
}

int32 osa_utils_queget(osa_utils_quehandle *handle, void **data,
                   uint32 minCount, uint32 timeout)
{
    int32 status = OSA_STATUS_EFAIL;/* init status to error */
    uint32 cookie;
    volatile osa_bool doBreak = (osa_bool)FALSE;

	if ((uint32) osa_semaphore_wait_forever == timeout) {
		timeout = SemaphoreP_WAIT_FOREVER;
	}
	else if ((uint32) osa_semaphore_no_wait == timeout) {
		timeout = SemaphoreP_NO_WAIT;
	}

    /*
     * adjust minCount between 1 and handle->maxElements
     */
    if (0U == minCount)
    {
        minCount = 1U;
    }
    if (minCount > handle->maxElements)
    {
        minCount = handle->maxElements;
    }

    do
    {
        /*
         * disable interrupts
         */
        cookie = HwiP_disable();

        if (handle->count >= minCount)
        {
            /*
             * data elements available in que is >=
             * minimum data elements requested by user
             */

            /*
             * extract the element
             */
            *data = handle->queue[handle->curRd];

            /*
             * increment get pointer
             */
            handle->curRd = (handle->curRd + 1) % handle->maxElements;

            /*
             * decrmeent number of elements in que
             */
            handle->count--;

            /*
             * restore interrupts
             */
            HwiP_restore(cookie);

            /*
             * set status as success
             */
            status = OSA_STATUS_SOK;

            if (handle->flags & OSA_UTILS_QUE_FLAG_BLOCK_QUE_PUT)
            {
                /*
                 * blocking on que put enabled
                 */
                if ((handle->count + 1U) == handle->maxElements)
                {
                    /*
                     * post semaphore to unblock, blocked tasks
                     */
                    SemaphoreP_post(handle->semWr);
                }
            }

            /*
             * exit with success
             */
            doBreak = (osa_bool)TRUE;

        }
        else
        {
            /*
             * no elements or not enough element (minCount) in que to extract
             */

            /*
             * restore interrupts
             */
            HwiP_restore(cookie);

            if (timeout == SemaphoreP_NO_WAIT)
            {
                doBreak = (osa_bool)TRUE;                      /* non-blocking
                                                            * function call,
                                                            * exit with error
                                                            */
                status = OSA_STATUS_EFAIL;
            }
            else
            if (handle->flags & OSA_UTILS_QUE_FLAG_BLOCK_QUE_GET)
            {
                osa_bool semPendStatus;

                /*
                 * blocking on que get enabled
                 */

                /*
                 * take semaphore and block until timeout occurs or
                 * semaphore is posted
                 */

                handle->blockedOnGet = (osa_bool)TRUE;
                semPendStatus = SemaphoreP_pend(handle->semRd, timeout);
                handle->blockedOnGet = (osa_bool)FALSE;
                if (!semPendStatus)
                {
                    /* UNREACH.GEN :  MISRAC_2004_Rule_14.1
                     * Unreachable Code
                     * MISRAC_WAIVER:
                     * Value in the if condition is dependent on the
                     * return of a function SemaphoreP_pend and
                     * this function is implemented when OS BIOS is
                     * included/defined.
                     */
                    handle->forceUnblockGet = (osa_bool)FALSE;
                    doBreak = (osa_bool)TRUE;                  /* timeout
                                                            * happened, exit
                                                            * with error */
                    status = OSA_STATUS_ETIMEOUT;
                }
                else if (handle->forceUnblockGet == (osa_bool)TRUE)
                {
                    handle->forceUnblockGet = (osa_bool)FALSE;
                    doBreak = (osa_bool)TRUE;                  /* timeout
                                                            * happened, exit
                                                            * with error */
                    status = OSA_STATUS_ETIMEOUT;
                }
                else
                {
                    doBreak = (osa_bool)FALSE;
                }
                /*
                 * received semaphore, check que again
                 */
            }
            else
            {
                /*
                 * blocking on que get disabled
                 */

                /*
                 * exit with error
                 */
                doBreak = (osa_bool)TRUE;
            }
        }

        if ((osa_bool)TRUE == doBreak)
        {
            break;
        }
    }
    while (1);

    return status;
}

int32 osa_utils_quereset(osa_utils_quehandle *handle)
{
    uint32 cookie;
    int32 status = OSA_STATUS_SOK;
    /*
     * disable interrupts
     */
    cookie = HwiP_disable();

    /*
     * Reset the queue
     */
    handle->count = 0;
    handle->curRd = 0;
    handle->curWr = 0;
    handle->blockedOnGet = (osa_bool)FALSE;
    handle->blockedOnPut = (osa_bool)FALSE;
    handle->forceUnblockGet = (osa_bool)FALSE;
    handle->forceUnblockPut = (osa_bool)FALSE;

    /*
     * restore interrupts
     */
    HwiP_restore(cookie);

    return status;
}


uint32 osa_utils_queisempty(const osa_utils_quehandle * handle)
{
	uint32 isEmpty;
	uint32 cookie;

    /*
     * disable interrupts
     */
    cookie = HwiP_disable();

    /*
     * check if que is empty
     */
    if (handle->count)
    {
        isEmpty = (uint32)FALSE;                     /* not empty */
    }
    else
    {
        isEmpty = (uint32)TRUE;                      /* empty */
    }

    /*
     * restore interrupts
     */
    HwiP_restore(cookie);

    return isEmpty;
}
