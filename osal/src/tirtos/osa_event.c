// SPDX-License-Identifier: GPL-2.0
/*
 * osa event implementation for TIRTOS
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2019 Texas Instruments Incorporated - http://www.ti.com/
 */

#include "../../inc/osa_define.h"
#include "../../inc/osa_event.h"
#include "../../inc/osa_mem.h"
#include "../../inc/osa_string.h"
#include "../../inc/osa_semaphore.h"
#include <ti/osal/EventP.h>
#include "img_errors.h"

int32 osa_create_event_obj(void ** event)
{
	EventP_Handle event_handle;
	event_handle =  EventP_create(NULL);

	*event = (void *)event_handle;

	return IMG_SUCCESS;
}

void osa_destroy_event_obj(void * event)
{
	EventP_delete(event);
}

int32 osa_wait_event_obj(void *event, osa_bool uninterruptible, uint32 timeout)
{
	uint32 ret_val;
	ret_val = EventP_pend(event, EventP_ID_NONE, EventP_ID_00, 0);
	if (ret_val == 0) {
		return IMG_ERROR_TIMEOUT;
	} else {
		return IMG_SUCCESS;
	}
}

void osa_signal_event_obj(void *event)
{
	EventP_post(event, EventP_ID_00);
}
