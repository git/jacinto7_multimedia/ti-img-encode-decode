// SPDX-License-Identifier: GPL-2.0
/*
 * osa work queue handling for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <ti/osal/SemaphoreP.h>
#include <ti/osal/osal.h>
#include "../../inc/osa_workqueue.h"
#include "../../inc/osa_mem.h"
#include "../../inc/osa_define.h"
#include "../../inc/osa_queue.h"
#include "../../inc/osa_semaphore.h"
#include "../../inc/osa_time.h"
#include "vxd_dec.h"
#include "vxe_enc.h"
#include "topaz_device.h"
#include "topaz_api.h"

enum osa_timer_status_e
{
	osa_timerp_idle,
	osa_timerp_start
};

void osa_timer_isr(void *arg)
{
	struct work_struct *work = (struct work_struct *)arg;

	osa_utils_queput(work->queue_handle, work, osa_semaphore_no_wait);
	work->timer_status = osa_timerp_idle;
}

static void *osa_create_timer(int32 id, void *timer_isr, void *arg)
{
	TimerP_Params timerParams;
	TimerP_Handle handle;

	TimerP_Params_init(&timerParams);
	timerParams.runMode = TimerP_RunMode_ONESHOT;
	timerParams.startMode = TimerP_StartMode_USER;
	timerParams.periodType = TimerP_PeriodType_MICROSECS;
	timerParams.period = 1000000U;
	timerParams.arg = arg;

	handle = TimerP_create(id, (TimerP_Fxn)timer_isr, &timerParams);

	if (handle == NULL) {
		OSA_PR_ERR("%s : Timer Create error \n", __func__);
	}

	return handle;
}

void osa_init_work(void **work_args, void *work_fn, uint8_t hwa_id)
{
	struct work_struct **work = (struct work_struct **)work_args;
	*work = osa_calloc(1, sizeof(struct work_struct), 0);
	if(NULL == *work) {
		OSA_PR_ERR("%s : Memory allocation failed for work_queue\n", __func__);
		return;
	}

	switch (hwa_id) {
		case HWA_DECODER:
		{
			struct vxd_dec_ctx *ctx = NULL;
			ctx = osa_container_of(work, struct vxd_dec_ctx, work);

			(*work)->worker_func = (void (*)( void *))work_fn;
			(*work)->work = work_args;
			(*work)->work_complete = FALSE;
			(*work)->queue_handle = ctx->stream_worker_queue_handle;
			(*work)->workq_sem_handle = ctx->stream_worker_queue_sem_handle;
			(*work)->lock = ctx->lock;
		}
		break;

		case HWA_ENCODER:
		{
		struct topaz_stream_context *str_ctx = NULL;
		struct img_comm_socket *sock;
		sock = osa_container_of(work, struct img_comm_socket, work);

		str_ctx = sock->str_ctx;

		(*work)->worker_func = (void (*)( void *))work_fn;
		(*work)->work = work_args;
		(*work)->work_complete = FALSE;
		(*work)->queue_handle = str_ctx->vxe_ctx->stream_worker_queue_handle;
		(*work)->workq_sem_handle = str_ctx->vxe_ctx->stream_worker_queue_sem_handle;
		(*work)->lock = str_ctx->vxe_ctx->mutex;
		}
		break;

		default:
			return;
	}
	return;
}

void osa_init_delayed_work(void **work_args, void *work_fn, uint8_t hwa_id)
{
	struct work_struct **work = (struct work_struct **)work_args;
	*work = osa_calloc(1, sizeof(struct work_struct), 0);
	if(NULL == *work) {
		OSA_PR_ERR("%s : ERR Memory allocation failed for delayed work_queue\n", __func__);
		return;
	}

	switch (hwa_id) {
		case HWA_DECODER:
		{
		struct vxd_dev *vxd = NULL;

		vxd = osa_container_of(work, struct vxd_dev, dwork);

		(*work)->worker_func = (void (*)( void *))work_fn;
		(*work)->work = work;
		(*work)->work_complete = FALSE;
		(*work)->queue_handle = vxd->vxd_worker_queue_handle;
		(*work)->workq_sem_handle = vxd->vxd_worker_queue_sem_handle;
		(*work)->timer_handle = osa_create_timer(TimerP_ANY, osa_timer_isr, *work);
		(*work)->timer_status = osa_timerp_idle;
		}
		break;

		case HWA_ENCODER:
			//to be implemented. Not required right now.
			break;

		default:
			return;
	}

	return;
}

int32 osa_schedule_work(void *work_args)
{
	struct work_struct *work = (struct work_struct *)work_args;

	osa_utils_queput(work->queue_handle, work, (uint32)osa_semaphore_wait_forever);

	return 0;
}

int32 osa_schedule_delayed_work(void *dwork, uint64 delay)
{
	int32 ret = 0;
	struct work_struct *work = (struct work_struct *)dwork;
	TimerP_Status timerStatus;

	if(osa_timerp_idle == work->timer_status)
	{
		work->timer_status = osa_timerp_start;
		TimerP_setPeriodMicroSecs(work->timer_handle, osa_jiffies_to_msecs(delay) * 1000);
		timerStatus = TimerP_start(work->timer_handle);
		if (timerStatus != TimerP_OK) {
			OSA_PR_ERR("%s : ERR Coult not start the timer \n", __func__);
		}
		ret = osa_false;
	}
	else
	{
		TimerP_stop(work->timer_handle);
		TimerP_setPeriodMicroSecs(work->timer_handle, osa_jiffies_to_msecs(delay) * 1000);
		timerStatus = TimerP_start(work->timer_handle);
		if (timerStatus != TimerP_OK) {
			OSA_PR_ERR("%s : ERR Coult not start the timer \n", __func__);
		}
		ret = osa_true;
	}

	return ret;
}

int32 osa_flush_work(void *work_args)
{
	struct work_struct *work = NULL;

	work = osa_malloc(sizeof(struct work_struct), 0);
	if(NULL != work)
	{
		memcpy(work, work_args, sizeof(struct work_struct));
		work->work_complete = TRUE;
		osa_utils_queput(work->queue_handle, work, (uint32)osa_semaphore_wait_forever);
		SemaphoreP_pend(work->workq_sem_handle, SemaphoreP_WAIT_FOREVER);
	}

    return 0;
}

int32 osa_cancel_delayed_work_sync(void *dwork)
{
	struct work_struct *work = NULL;
	TimerP_Status timerStatus;

	work = osa_malloc(sizeof(struct work_struct), 0);
	if(NULL != work)
	{
		memcpy(work, dwork, sizeof(struct work_struct));
		TimerP_stop(work->timer_handle);
		osa_utils_quereset(work->queue_handle);
		work->work_complete = TRUE;
		osa_utils_queput(work->queue_handle, work, (uint32)osa_semaphore_wait_forever);

		timerStatus = TimerP_delete(work->timer_handle);
		if(timerStatus != TimerP_OK)
		{
			OSA_PR_ERR("%s : Timer delete error \n", __func__);
		}
	}

	return 0;
}

int32 osa_cancel_delayed_work(void *dwork)
{
	struct work_struct *work = (struct work_struct *)dwork;

	osa_utils_quereset(work->queue_handle);
	TimerP_stop(work->timer_handle);
	work->timer_status = osa_timerp_idle;

	return 0;
}

int32 osa_mod_delayed_work(void*wq, void *dwork, uint64 delay)
{
	struct work_struct *work = (struct work_struct *)dwork;
	int32 ret = osa_true;
	TimerP_Status timerStatus;

	TimerP_stop(work->timer_handle);
	TimerP_setPeriodMicroSecs(work->timer_handle, osa_jiffies_to_msecs(delay) * 1000);
	timerStatus = TimerP_start(work->timer_handle);
	if (timerStatus != TimerP_OK) {
		OSA_PR_ERR("%s : ERR Coult not start the timer \n", __func__);
		ret = osa_false;
	}

	return ret;
}

void *osa_get_global_workqueue_struct(void)
{
	/* Dummy implementation for TI RTOS */
    return NULL;
}

void *osa_get_work_buff(void *key, int8 flag)
{
	return key;
}

void *osa_get_delayed_work_buff(void *key, int8 flag)
{
	return key;
}
