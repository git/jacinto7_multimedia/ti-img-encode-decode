// SPDX-License-Identifier: GPL-2.0
/*
 * osa delay functions implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <string.h>

#include "../../inc/osa_define.h"
#include "../../inc/osa_delay.h"

void osa_udelay(uint64 usecs)
{
    uint32 msecs;

    msecs = (usecs/1000U)+1;
    TaskP_sleepInMsecs(msecs);
}

void osa_msleep(uint32 msecs)
{
    TaskP_sleepInMsecs(msecs);
}

void osa_usleep_range(uint64 min, uint64 max)
{
    /* As this feature not supported in TIRTOS, sleep for max time */
    uint32 msecs;

    msecs = (max/1000U)+1;
    TaskP_sleepInMsecs(msecs);
}

void osa_ndelay(uint64 nsecs)
{
    uint32 msecs;

    msecs = (nsecs/1000000U)+1;
    TaskP_sleepInMsecs(msecs);
}
