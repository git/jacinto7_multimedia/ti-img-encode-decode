// SPDX-License-Identifier: GPL-2.0
/*
 * osa time handling functions implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <ti/osal/TaskP.h>
#include <ti/sysbios/knl/Task.h>
#include "osa_task.h"

/* Note:
 * INTERRUPT_TASK_PRIORITY > VXD_WORKER_TASK_PRIORITY &
 * VXD_WORKER_TASK_PRIORITY > application task priority &
 * STREAM_WORKER_TASK_PRIORITY < application task priority
 *
 * application task priority is 8 in our application
 * */
#define INTERRUPT_TASK_PRIORITY		10

#define VXD_WORKER_TASK_PRIORITY	9

#define STREAM_WORKER_TASK_PRIORITY	9

void *osa_task_create(void *taskfxn, task_params *params)
{
	TaskP_Params tsk_params;
	void *task_handle = NULL;

	TaskP_Params_init(&tsk_params);
	tsk_params.arg0 = params->arg0;
	tsk_params.arg1 = params->arg1;
	switch(params->priority)
	{
	case OSA_INTERRUPT_TASK_PRIORITY:
		tsk_params.priority = INTERRUPT_TASK_PRIORITY;
		break;
	case OSA_VXD_WORKER_TASK_PRIORITY:
		tsk_params.priority = VXD_WORKER_TASK_PRIORITY;
		break;
	case OSA_STREAM_WORKER_TASK_PRIORITY:
		tsk_params.priority = STREAM_WORKER_TASK_PRIORITY;
		break;
	}
	tsk_params.name = (uint8_t *)params->name;
	tsk_params.stacksize = params->stacksize;
	tsk_params.stack = params->stack;

	task_handle = TaskP_create(taskfxn, &tsk_params);

	return task_handle;
}

int32 osa_task_delete(void **handle)
{
	if (Task_deleteTerminatedTasks == FALSE)
		return TaskP_delete(handle);
	else
		return 0;
}

void osa_task_sleep(uint32 timeout)
{
	TaskP_sleep(timeout);
}
