// SPDX-License-Identifier: GPL-2.0
/*
 * osa mutex handling implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <string.h>

#include <ti/osal/TaskP.h>
#include <ti/osal/SemaphoreP.h>
#include "../../inc/osa_mutex.h"
#include "../../inc/osa_mem.h"
#include "../../inc/osa_define.h"

void osa_mutex_create(void **mutex_args)
{
    SemaphoreP_Params semParams;
    SemaphoreP_Handle *mutexHndl = (SemaphoreP_Handle *)mutex_args;

	SemaphoreP_Params_init(&semParams);
	semParams.mode = SemaphoreP_Mode_BINARY;
    *mutexHndl = SemaphoreP_create(1U, &semParams);
    if (NULL == *mutexHndl)
	{
		OSA_PR_ERR("REMOTE_SERVICE: Unable to create tx semaphore\n");
	}

    return;
}

void osa_mutex_unlock(void *mutex_args)
{
    SemaphoreP_Handle mutexHndl;
    mutexHndl = (SemaphoreP_Handle)mutex_args;

    if (NULL != mutexHndl)
    {
        SemaphoreP_post(mutexHndl);
    }

    return;
}

void osa_mutex_lock(void *mutex_args)
{
    SemaphoreP_Handle mutexHndl;
    mutexHndl = (SemaphoreP_Handle)mutex_args;

    if (NULL != mutexHndl)
    {
        SemaphoreP_pend(mutexHndl, SemaphoreP_WAIT_FOREVER);
    }

    return;
}

void osa_mutex_lock_nested(void *mutex_args, uint32 subclass)
{
    return osa_mutex_lock(mutex_args);
}

void osa_mutex_destroy(void **mutex_args)
{
    SemaphoreP_Handle *mutexHndl;
    mutexHndl = (SemaphoreP_Handle*)mutex_args;

    if (NULL != *mutexHndl)
    {
        SemaphoreP_delete(*mutexHndl);
        *mutexHndl = NULL;
    }

    return;
}

int32 osa_mutex_lockinterruptible(void *mutex_args)
{
	/* Just calling osa_mutex_lock as lockinterruptible not supported in TIRTOS */
    osa_mutex_lock(mutex_args);
    return 0;
}

int32 osa_mutex_lockinterruptible_nested(void *mutex_args, uint32 subclass)
{
    return osa_mutex_lockinterruptible(mutex_args);
}

int32 osa_mutex_trylock(void *mutex_args)
{
	/* Dummy implementation for TI RTOS */
    return 0;
}

int32 osa_mutex_is_locked(void *mutex_args)
{
	return !SemaphoreP_getCount(mutex_args);
}

int32  osa_mutex_lock_killable(void *mutex_args)
{
	/* Dummy implementation for TI RTOS */
    return 0;
}

void  osa_mutex_lock_io(void *mutex_args)
{
	/* Dummy implementation for TI RTOS */
    return;
}
