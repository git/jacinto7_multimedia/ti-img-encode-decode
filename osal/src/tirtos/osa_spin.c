// SPDX-License-Identifier: GPL-2.0
/*
 * osa spinlock handling implementation for TIRTOS
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "../../inc/osa_spin.h"
#include "../../inc/osa_define.h"
#include "../../inc/osa_mem.h"
#include <ti/osal/HwiP.h>
#include "img_errors.h"

void osa_spin_lock_irqsave(void *lock, ulong *flags_args)
{
	uint32 cookie;

	/*
	 * disable interrupts
	 */
	cookie = HwiP_disable();
	*flags_args = cookie;
}

void osa_spin_unlock_irqrestore(void *lock, uint64 flags)
{
	/*
	 * restore interrupts
	 */
	HwiP_restore(flags);
}

void osa_spin_lock_create(void **lock_args, const int8 *name)
{
	/* dummy lock allocation to not to break common driver */
	uint32 **lock = (uint32 **)lock_args;

	*lock = osa_malloc(sizeof(uint32), 0);
}


void osa_spin_lock(void *lock)
{

}

int32 osa_spin_trylock(void *lock)
{
	return 0;
}

void osa_spin_unlock(void *lock)
{

}

int32 osa_spin_is_locked(void *lock)
{
	return 0;
}

void osa_spin_lock_irq(void *lock)
{

}

void osa_spin_unlock_irq(void *lock)
{

}

void osa_spin_destroy(void **lock_args)
{

	osa_free(*lock_args);

}
