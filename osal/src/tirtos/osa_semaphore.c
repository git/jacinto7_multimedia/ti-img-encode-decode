// SPDX-License-Identifier: GPL-2.0
/*
 * osa time handling functions implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "../../inc/osa_types.h"
#include "../../inc/osa_semaphore.h"

#include <ti/osal/SemaphoreP.h>

void *osa_semaphore_create(enum osa_semaphore_mode mode, uint32 count)
{
	SemaphoreP_Params semParams;
	void *Handle;

	/* Initialize the semaphore parameters and create semaphore pool */
	SemaphoreP_Params_init(&semParams);
	if(osa_semaphore_mode_counting == mode)
	{
		semParams.mode = SemaphoreP_Mode_COUNTING;
	}
	else
	{
		semParams.mode = SemaphoreP_Mode_BINARY;
	}
	Handle = SemaphoreP_create(count, &semParams);

	return Handle;
}

int32 osa_semaphore_delete(void *handle)
{
	return SemaphoreP_delete(handle);
}

int32 osa_semaphore_pend(void *handle, uint32 timeout)
{
	SemaphoreP_Status res;

	if((uint32)osa_semaphore_wait_forever == timeout)
	{
		res = SemaphoreP_pend(handle, SemaphoreP_WAIT_FOREVER);
	}
	else if ((uint32)osa_semaphore_no_wait == timeout)
	{
		res = SemaphoreP_pend(handle, SemaphoreP_NO_WAIT);
	}
	else
	{
		res = SemaphoreP_pend(handle, timeout);
	}

	return res;
}

int32 osa_semaphore_post(void *handle)
{
	return SemaphoreP_post(handle);
}
