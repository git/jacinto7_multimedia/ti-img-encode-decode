// SPDX-License-Identifier: GPL-2.0
/*
 * osa scatter gather page list handling implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <string.h>

#include "../../inc/osa_define.h"
#include "../../inc/osa_page.h"
#include "../../inc/osa_mem.h"

#define SG_CHAIN	0x01UL
#define SG_END		0x02UL

void osa_get_pages(void **page_args, void *sgt_args)
{
    struct page **pages = (struct page **)page_args;
    struct sg_table *sgt = sgt_args;
    struct scatterlist *sgl = sgt->sgl;
    int32 i;
    i = 0;
    while (sgl) {
        pages[i++] = osa_sg_page(sgl);
        sgl = osa_sg_next(sgl);
    }
}

void *osa_sg_page(void *sg)
{
    struct scatterlist *sgl = sg;
    /* As only one page for each buffer in TIRTOS, this should be NULL */
	return (struct page *)((sgl)->page_link & ~(SG_CHAIN | SG_END));
}

void osa_free_page(void *page)
{
    struct page *pagePtr = page;
    pagePtr->sgt = NULL;
    osa_free((void*)pagePtr);
}

void *osa_sg_next(void *sg)
{
	struct scatterlist *new_sg = sg;

	++new_sg;
	if(new_sg->dma_address == 0 && new_sg->length == 0)
	{
		return NULL;
	}
	else
	{
		return new_sg;
	}
}

void osa_sg_free_table(void *table)
{
	struct sg_table *sgt = table;
	if (sgt->sgl->is_alloc_inside == 1)
	{
		osa_free((void*)sgt->sgl->dma_address);
	}
	osa_free((void*)sgt->sgl);
    sgt->sgl = NULL;
}

void osa_sg_set_page(void *sg, void *page,
                   uint32 len, uint32 offset)
{
    struct scatterlist *sgl = sg;
    struct page *pagePtr = page;
	unsigned long page_link = sgl->page_link & (SG_CHAIN | SG_END);
	/*
	 * In order for the low bit stealing approach to work, pages
	 * must be aligned at a 32-bit boundary as a minimum.
	 */
	sgl->page_link = page_link | (unsigned long) pagePtr;
	sgl->offset = offset;
}

void osa_dma_unmap_page(void *dev,
                    uint64 addr, size_t size,
                    int32 dir)
{
	/* Nothing to do for TIRTOS */
    return;
}

int32 osa_dma_mapping_error(void *dev, uint64 dma_addr)
{
	/* Nothing to do for TIRTOS */
    return 0;
}

uint64 osa_dma_map_page(void *dev,
                        void *page,
                        size_t offset, size_t size,
                        int32 dir)
{
	uint64 addr = 0;
	struct sg_table *sgt = ((struct page *)page)->sgt;
	struct scatterlist *sgl = sgt->sgl;

	sgl->offset = offset;
	/* Assume Physical address is passed as dev pointer incase of TITOS */
	addr = (uint64) sgl->dma_address;

    return addr;
}

uint64 osa_page_to_phys(void *page)
{
	uint64 addr = 0;
	struct sg_table *sgt = ((struct page *)page)->sgt;
	struct scatterlist *sgl = sgt->sgl;

	/* Assume Physical address is stored in dma_address incase of TITOS */
	addr = (uint64) sgl->dma_address;
    return addr;
}

void *osa_alloc_page(int64 gfp_mask, void *sgt)
{
	int64 flags = 0;

	struct page *page = osa_malloc((uint64)sizeof(struct page), flags);
	page->sgt = (struct sg_table*) sgt;
    return (page);
}

int32 osa_sg_alloc_table(void *table, uint32 nents, int64 gfp_mask, size_t size, void *ptr)
{
	int ret = 0;
	struct sg_table *sgt = table;
	struct scatterlist *sgl;
	int64 flags = 0;

	if (nents < 1)
	{
		return -1;
	}

    sgt->nents = nents;
    sgt->orig_nents = nents -1;

	sgl = osa_malloc((uint64)sizeof(struct scatterlist) * (MAX_PLANES + 1), flags);
	memset(sgl, 0, sizeof(*sgl) * (MAX_PLANES + 1));

	sgt->sgl = sgl;
	sgl->page_link = 0;
	if (size!=0 && ptr==NULL)
	{
		sgl->dma_address = osa_malloc((uint64)size, flags);
		sgl->is_alloc_inside = 1;
	}
	else
	{
		sgl->dma_address = ptr;
		sgl->is_alloc_inside = 0;
	}
	sgl->offset = 0;
	sgl->length = size;

    return ret;
}

int32 osa_dma_map_sg(void *dev, void *sg,
                   int32 nents, int32 dir)
{
	/* Nothing to do for TIRTOS */

    return 1;
}

int32 osa_sg_nents(void *sg)
{
    struct page *page = (struct page *)osa_sg_page(sg);
    return page->sgt->nents;
}

void *osa_get_sgl(void *sgt_args)
{
    struct sg_table *sgt = sgt_args;
    return sgt->sgl;
}

uint32 osa_get_orig_nents(void *sgt_args)
{
    struct sg_table *sgt = sgt_args;
    return sgt->orig_nents;
}

uint32 osa_get_size_sgt(void)
{
    return sizeof(struct sg_table);
}

void osa_set_sgt_nents(void *sgt_args, int32 ret)
{
    struct sg_table *sgt = sgt_args;
    sgt->nents = ret;
}

void osa_dma_unmap_sg(void *dev, void *sg, int32 nents, int32 dir)
{
	/* Nothing to do for TIRTOS */

    return;
}

void osa_dma_sync_sg_for_cpu(void *dev, void *sg, int32 nelems, int32 dir)
{
	/* Nothing to do for TIRTOS */
    return;
}

void osa_dma_sync_sg_for_device(void *dev, void *sg, int32 nelems, int32 dir)
{
	/* Nothing to do for TIRTOS */
    return;
}

void osa_set_sg_table(void **sg_table_args, void *buffer)
{
    struct sg_table **sg_table = (struct sg_table **)sg_table_args;
    *sg_table = buffer;
}

uint64 osa_sg_phys(void *sg)
{
    struct scatterlist *sgl = sg;
    struct page *pagePtr = osa_sg_page(sgl);

	return ((uint64)sgl->dma_address + sgl->offset);
}

uint32 osa_get_sgl_length(void *sgl_args)
{
    struct scatterlist *sgl = (struct scatterlist *)sgl_args;
    return sgl->length;
}

void osa_mb(void)
{
	/* Nothing to do for TIRTOS */
    return;
}
