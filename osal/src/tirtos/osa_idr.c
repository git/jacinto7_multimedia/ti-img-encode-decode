// SPDX-License-Identifier: GPL-2.0
/*
 * osa idr list handling implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>

#include "../../inc/osa_idr.h"
#include "../../inc/osa_err.h"
#include "../../inc/osa_mem.h"
#include "../../inc/osa_define.h"

struct idr {
    uint32          *idr_array;
    uint32          idr_start;
    uint32          idr_end;
    uint32          idr_current;
    osa_bool        init_completion;
};

int32 osa_idr_alloc(void *idr, void *ptr, int32 start, int32 end)
{
    struct idr *idr_new = (struct idr *)idr;
    int i = 0;

    if(NULL == idr_new->idr_array && !(idr_new->init_completion))
    {
        idr_new->idr_array = osa_calloc(end + 1, sizeof(void *), 0);
        if (NULL == idr_new->idr_array)
        {
            OSA_PR_ERR("Memory allocation failed in %s\n", __func__);
            return -OSA_ENOMEM;
        }
        idr_new->idr_start = start;
        idr_new->idr_end = end;
        idr_new->idr_current = start;
        idr_new->init_completion = 1;
    }

    for (i = idr_new->idr_current; i <= end; i++)
    {
        if(NULL != idr_new->idr_array)
        {
            if(NULL == idr_new->idr_array[i])
            {
                idr_new->idr_current = i;
                idr_new->idr_array[i] = (uint32)ptr;
                return i;
            }
        }
        else
        {
            OSA_PR_ERR("Memory deference failed in %s\n", __func__);
        }
    }
    for (i = start; i < idr_new->idr_current; i++)
    {
        if(NULL != idr_new->idr_array)
        {
            if(NULL == idr_new->idr_array[i])
            {
                idr_new->idr_current = i;
                idr_new->idr_array[i] = (uint32)ptr;
                return i;
            }
        }
        else
        {
            OSA_PR_ERR("Memory deference failed in %s\n", __func__);
        }
    }

    return -OSA_ENOMEM;
}

int32 osa_idr_alloc_cyclic(void *idr, void *ptr, int32 start, int32 end)
{
    return osa_idr_alloc(idr, ptr, start, end);
}

void *osa_idr_remove(void *idr, int32 id)
{
    struct idr *idr_new = (struct idr *)idr;
    void *temp = (void *)idr_new->idr_array[id];
    idr_new->idr_array[id] = NULL;
    return temp;
}

void *osa_idr_find(void *idr, int32 id)
{
    struct idr *idr_new = (struct idr *)idr;
    return (void *)idr_new->idr_array[id];
}

void *osa_idr_get_next(void *idr, int32 *nextid)
{
    struct idr *idr_new = (struct idr *)idr;
    int32 i = 0;

    if(idr_new)
    {
        for(i = *nextid + 1; i <= idr_new->idr_end; i++)
        {
            if(NULL != idr_new->idr_array[i])
            {
                *nextid = i;
                return (void *)idr_new->idr_array[i];
            }
        }

        if(NULL != idr_new->idr_array[*nextid])
        {
            return (void *)idr_new->idr_array[*nextid];
        }
    }
    return NULL;
}

void osa_idr_destroy(void *idr)
{
    struct idr *idr_new = (struct idr *)idr;
    if(NULL != idr)
    {
        idr_new->init_completion = 0;
        osa_free(idr_new->idr_array);
        osa_free(idr);
    }
}

void osa_idr_init(void **idr_args)
{
    struct idr **idr = (struct idr **)idr_args;
    *idr = osa_calloc(1, sizeof(struct idr), 0);
    if (NULL == *idr)
    {
        OSA_PR_ERR("Memory allocation failed in %s\n", __func__);
        return;
    }
}
