// SPDX-License-Identifier: GPL-2.0
/*
 * osa scatter gather page list handling implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/dma-mapping.h>

#include "../../inc/osa_page.h"

void osa_get_pages(void **page_args, void *sgt_args)
{
    struct page **pages = (struct page **)page_args;
    struct sg_table *sgt = sgt_args;
    struct scatterlist *sgl = sgt->sgl;
    int32 i;
    i = 0;
    while (sgl) {
        pages[i++] = sg_page(sgl);
        sgl = sg_next(sgl);
    }
}

void *osa_sg_page(void *sgl)
{
    return sg_page(sgl);
}

void osa_free_page(void *page)
{
    __free_page(page);
}

void *osa_sg_next(void *sg)
{
    return sg_next(sg);
}

void osa_sg_free_table(void *table)
{
    sg_free_table(table);
}

void osa_sg_set_page(void *sg, void *page,
                   uint32 len, uint32 offset)
{
    sg_set_page(sg, page, len, offset);
}

void osa_dma_unmap_page(void *dev,
                    uint64 addr, size_t size,
                    int32 dir)
{
    dma_unmap_page(dev, addr, size, dir);
}

int32 osa_dma_mapping_error(void *dev, uint64 dma_addr)
{
    return dma_mapping_error(dev, dma_addr);
}

uint64 osa_dma_map_page(void *dev,
                        void *page,
                        size_t offset, size_t size,
                        int32 dir)
{
    return dma_map_page(dev, page, offset, size, dir);
}

uint64 osa_page_to_phys(void *page)
{
    return page_to_phys((struct page *)page);
}

void *osa_alloc_page(int64 gfp_mask, void *sgt)
{
    return alloc_page(gfp_mask);
}

int32 osa_sg_alloc_table(void *table, uint32 nents, int64 gfp_mask, size_t size, void *ptr)
{
    return sg_alloc_table(table, nents, gfp_mask);
}

int32 osa_dma_map_sg(void *dev, void *sg,
                   int32 nents, int32 dir)
{
    return dma_map_sg(dev, sg, nents, dir);
}

int32 osa_sg_nents(void *sg)
{
    return sg_nents(sg);
}

void *osa_get_sgl(void *sgt_args)
{
    struct sg_table *sgt = sgt_args;
    return sgt->sgl;
}

uint32 osa_get_orig_nents(void *sgt_args)
{
    struct sg_table *sgt = sgt_args;
    return sgt->orig_nents;
}

uint32 osa_get_size_sgt(void)
{
    return sizeof(struct sg_table);
}

void osa_set_sgt_nents(void *sgt_args, int32 ret)
{
    struct sg_table *sgt = sgt_args;
    sgt->nents = ret;
}

void osa_dma_unmap_sg(void *dev, void *sg, int32 nents, int32 dir)
{
    dma_unmap_sg(dev, sg, nents, dir);
}

void osa_dma_sync_sg_for_cpu(void *dev, void *sg, int32 nelems, int32 dir)
{
    dma_sync_sg_for_cpu(dev, sg, nelems, dir);
}

void osa_dma_sync_sg_for_device(void *dev, void *sg, int32 nelems, int32 dir)
{
    dma_sync_sg_for_device(dev, sg, nelems, dir);
}

void osa_set_sg_table(void **sg_table_args, void *buffer)
{
    struct sg_table **sg_table = (struct sg_table **)sg_table_args;
    *sg_table = buffer;
}

uint64 osa_sg_phys(void *sg)
{
    return sg_phys(sg);
}

uint32 osa_get_sgl_length(void *sgl_args)
{
    struct scatterlist *sgl = (struct scatterlist *)sgl_args;
    return sgl->length;
}

void osa_mb(void)
{
    mb();
}
