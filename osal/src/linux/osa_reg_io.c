// SPDX-License-Identifier: GPL-2.0
/*
 * osa reg io handling implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2019 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/io.h>
#include <linux/slab.h>
#include "../../inc/osa_delay.h"
#include "../../inc/osa_reg_io.h"


void osa_reg32_write(uint32 val, uint_addr addr)
{
	iowrite32(val, (void *) addr);
}

uint32 osa_reg32_read(uint_addr addr)
{
	return ioread32( (void *) addr);
}

int32 osa_reg32_poll_iseq(uint_addr addr, uint32 req_val, uint32 mask, uint32 cnt)
{
	uint32 count, val;
	uint32 res = 0;

	/* Add high-frequency poll loops. */
	cnt += 10;

	/*
	 * High-frequency loop (designed for shorter hardware latency such as
	 * reset).
	 */
	for (count = 0; count < cnt; count++) {
		/* Read from the device */
		val = osa_reg32_read(addr);
		val = (val & mask);

		if (val == req_val) {
			res = 0;
			break;
		}

		/*
		 * Sleep to wait for hardware.
		 * Period is selected to allow for high-frequency polling
		 * (5us, e.g. reset) over the first 10 iterations, then
		 * reverting to a lower-frequency (100us, e.g. DMA) for the
		 * remainder.
		 */
		if (count < 10)
			osa_usleep_range(5, 5);
		else
			osa_usleep_range(100, 100);
	}

	if (res || (count >= cnt)) {
		pr_info("Poll failed!\n");
		res = -1;
	}

	return res;
}
