// SPDX-License-Identifier: GPL-2.0
/*
 * osa atomic implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2019 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/slab.h>
#include <linux/printk.h>
#include <asm/atomic.h>
#include "../../inc/osa_atomic.h"
#include "../../inc/osa_mem.h"
#include "img_errors.h"


int32 osa_atomic_create(void **atomic_args)
{
    atomic_t **obj = (atomic_t **)atomic_args;
    *obj = osa_zalloc(sizeof(atomic_t), GFP_KERNEL);
    if (NULL == *obj) {
        pr_err("Memory allocation failed for atomic\n");
        return IMG_ERROR_OUT_OF_MEMORY;
    }
    atomic_set(*obj, 0);
    return IMG_SUCCESS;
}

void osa_atomic_destroy(void **atomic_args)
{
    atomic_t **obj = (atomic_t **)atomic_args;
    if (!obj || !*obj) {
        pr_err("Invalid atomic object\n");
        return;
    }
    osa_free(*obj);
    *obj = NULL;
}

int32 osa_atomic_read(void *atomic_args)
{
    return atomic_read((atomic_t *)atomic_args);
}

void osa_atomic_set(void *atomic_args, int32 val)
{
    atomic_set((atomic_t *)atomic_args, val);
}

int32 osa_atomic_inc_ret(void *atomic_args)
{
    return atomic_inc_return((atomic_t *)atomic_args);
}

int32 osa_atomic_dec_ret(void *atomic_args)
{
    return atomic_dec_return((atomic_t *)atomic_args);
}
