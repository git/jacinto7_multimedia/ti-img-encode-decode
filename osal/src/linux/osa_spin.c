// SPDX-License-Identifier: GPL-2.0
/*
 * osa spinlock handling implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/spinlock.h>
//#include <linux/spinlock_types.h>
#include <linux/slab.h>

#include "../../inc/osa_spin.h"
#include "../../inc/osa_mem.h"



void osa_spin_lock_create(void **lock_args, const int8 *name)
{
    spinlock_t **lock = (spinlock_t **)lock_args;
    *lock = osa_zalloc(sizeof(spinlock_t), GFP_KERNEL);
    if (NULL == *lock)
    {
        pr_err("Memory allocation failed for spin-lock\n");
        return;
    }
    spin_lock_init(*lock);
}


void osa_spin_lock(void *lock)
{
    return spin_lock(lock);
}

int32 osa_spin_trylock(void *lock)
{
    return spin_trylock(lock);
}

void osa_spin_unlock(void *lock)
{
    return spin_unlock(lock);
}

int32 osa_spin_is_locked(void *lock)
{
    return spin_is_locked(lock);
}

void osa_spin_lock_irqsave(void *lock, ulong *flags_args)
{
    ulong flag = 0;
    spin_lock_irqsave(lock, flag);
    *flags_args = flag;
}

void osa_spin_unlock_irqrestore(void *lock, uint64 flags)
{
    spin_unlock_irqrestore(lock, (unsigned long)flags);
}

void osa_spin_lock_irq(void *lock)
{
    return spin_lock_irq(lock);
}

void osa_spin_unlock_irq(void *lock)
{
    return spin_unlock_irq(lock);
}

void osa_spin_destroy(void **lock_args)
{
    osa_free(*lock_args);
}

