// SPDX-License-Identifier: GPL-2.0
/*
 * osa idr list handling implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/idr.h>
#include <linux/slab.h>

#include "../../inc/osa_idr.h"
#include "../../inc/osa_mem.h"


int32 osa_idr_alloc(void *idr, void *ptr, int32 start, int32 end)
{
    return idr_alloc(idr, ptr, start, end, GFP_KERNEL);
}

int32 osa_idr_alloc_cyclic(void *idr, void *ptr, int32 start, int32 end)
{
    return idr_alloc_cyclic(idr, ptr, start, end, GFP_KERNEL);
}

void *osa_idr_remove(void *idr, int32 id)
{
    return idr_remove(idr, id);
}

void *osa_idr_find(void *idr, int32 id)
{
    return idr_find(idr, id);
}

void *osa_idr_get_next(void *idr, int32 *nextid)
{
    return idr_get_next(idr, nextid);
}

void osa_idr_destroy(void *idr)
{
    idr_destroy(idr);
    osa_free(idr);
}

void osa_idr_init(void **idr_args)
{
    struct idr **idr = (struct idr **)idr_args;
    *idr = osa_zalloc(sizeof(struct idr), GFP_KERNEL);
    if (NULL == *idr)
    {
        pr_err("Memory allocation failed for idr\n");
        return;
    }
    idr_init(*idr);
}
