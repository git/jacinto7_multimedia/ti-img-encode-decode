// SPDX-License-Identifier: GPL-2.0
/*
 * osa mutex handling implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/slab.h>
#include <linux/printk.h>
#include <linux/mutex.h>
#include "../../inc/osa_mutex.h"
#include "../../inc/osa_mem.h"


void osa_mutex_create(void **mutex_args)
{
    struct mutex **mutex = (struct mutex **)mutex_args;
    *mutex = osa_zalloc(sizeof(struct mutex), GFP_KERNEL);
    if (NULL == *mutex)
    {
        pr_err("Memory allocation failed for mutex\n");
        return;
    }
    mutex_init(*mutex);
}

void osa_mutex_unlock(void *mutex_args)
{
    return mutex_unlock((struct mutex *)mutex_args);
}

void osa_mutex_lock(void *mutex_args)
{
    return mutex_lock((struct mutex *)mutex_args);
}

void osa_mutex_lock_nested(void *mutex_args, uint32 subclass)
{
    return mutex_lock_nested(mutex_args, subclass);
}

void osa_mutex_destroy(void **mutex_args)
{
    struct mutex **mutex = (struct mutex **)mutex_args;
    mutex_destroy(*mutex);
    osa_free(*mutex);
    *mutex = NULL;
}

int32 osa_mutex_lockinterruptible(void *mutex_args)
{
    return mutex_lock_interruptible(mutex_args);
}

int32 osa_mutex_lockinterruptible_nested(void *mutex_args, uint32 subclass)
{
    return mutex_lock_interruptible_nested(mutex_args, subclass);
}

int32 osa_mutex_trylock(void *mutex_args)
{
    return mutex_trylock(mutex_args);
}

int32 osa_mutex_is_locked(void *mutex_args)
{
    return mutex_is_locked((struct mutex *)mutex_args);
}

int32  osa_mutex_lock_killable(void *mutex_args)
{
    return mutex_lock_killable(mutex_args);
}

void  osa_mutex_lock_io(void *mutex_args)
{
    return mutex_lock_io(mutex_args);
}

