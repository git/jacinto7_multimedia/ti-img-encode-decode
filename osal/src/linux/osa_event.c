// SPDX-License-Identifier: GPL-2.0
/*
 * osa event implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2019 Texas Instruments Incorporated - http://www.ti.com/
 */

#include <linux/wait.h>
#include "../../inc/osa_define.h"
#include "../../inc/osa_event.h"
#include "../../inc/osa_mem.h"
#include "../../inc/osa_string.h"
#include "img_errors.h"


DECLARE_WAIT_QUEUE_HEAD (event_wait_queue);

/*!
******************************************************************************
 Event object structure
******************************************************************************/
struct osa_event
{
	osa_bool signalled;
};

int32 osa_create_event_obj(void ** event)
{
	struct osa_event * p_event;

	/* Allocate a Sync structure...*/
	p_event = osa_malloc(sizeof(*p_event), OSA_GFP_KERNEL);
	IMG_DBG_ASSERT(p_event != NULL);
	if (p_event == NULL)
		return IMG_ERROR_OUT_OF_MEMORY;

	osa_memset(p_event, 0, sizeof(*p_event));

	/* Return the event structure...*/
	*event = (void *)p_event;

    return IMG_SUCCESS;
}

void osa_destroy_event_obj(void * event)
{
	struct osa_event * p_event = (struct osa_event *)event;

	IMG_DBG_ASSERT(event != NULL);
	if (event == NULL)
		return;

	/* Free structure...*/
	osa_free(p_event);
}

int32 osa_wait_event_obj(void *event, osa_bool uninterruptible, uint32 timeout)
{
	struct osa_event *p_event = (struct osa_event *)event;
	int ret;

	IMG_DBG_ASSERT(event != NULL);
	if (event == NULL)
		return IMG_ERROR_GENERIC_FAILURE;

	if (uninterruptible) {
		if (timeout == (uint32)(-1)) {
			ret = 0;
			wait_event(event_wait_queue, p_event->signalled);
		}
		else {
			ret = wait_event_timeout(event_wait_queue, p_event->signalled, timeout);
			if (!ret)
				return IMG_ERROR_TIMEOUT;
		}
	}
	else {
		if (timeout == (uint32)(-1))
			ret = wait_event_interruptible(event_wait_queue, p_event->signalled);
		else {
			ret = wait_event_interruptible_timeout(event_wait_queue, p_event->signalled, timeout);
			if (!ret)
				return IMG_ERROR_TIMEOUT;
		}
	}

	/* If there are signals pending... */
	if (ret == -ERESTARTSYS)
		return IMG_ERROR_INTERRUPTED;

	/* If there was no signal...*/
	IMG_DBG_ASSERT(p_event->signalled == osa_true);

	/* Clear signal pending...*/
	p_event->signalled = osa_false;

	return IMG_SUCCESS;
}

void osa_signal_event_obj(void *event)
{
	struct osa_event *p_event = (struct osa_event *)event;

	IMG_DBG_ASSERT(event != NULL);
	if (event == NULL)
		return;

	p_event->signalled = osa_true;
	wake_up(&event_wait_queue);
}
