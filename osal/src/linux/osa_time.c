// SPDX-License-Identifier: GPL-2.0
/*
 * osa time handling functions implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/time.h>
#include <linux/jiffies.h>
#include <linux/slab.h>

#include "../../inc/osa_time.h"
#include "../../inc/osa_mem.h"

uint64 osa_get_number_of_ticks(void)
{
    return jiffies;
}

uint64 osa_msecs_to_jiffies(uint32 m)
{
    return msecs_to_jiffies(m);
}

uint64 osa_jiffies_to_msecs(uint64 j)
{
    return jiffies_to_msecs(j);
}

void osa_getnstimeofday(struct osa_timespec *ts_args)
{
    struct timespec *ts;
    struct timespec64 ts64;
    struct timespec time;

    if(NULL == ts_args)
    {
        pr_err("Failed to allocate memory in 'osa_getnstimeofday' function \n");
    }
    else
    {
        ts = (struct timespec *)(&ts_args->ts);
        ktime_get_real_ts64(&ts64);
        time = timespec64_to_timespec(ts64);
        memcpy(ts, &time, sizeof(struct timespec));
    }
}

void osa_timespec_sub(struct osa_timespec *end_time, struct osa_timespec *start_time, struct osa_timespec *dif_time)
{
    struct timespec *ts = (struct timespec *)(&dif_time->ts);

    *ts = timespec_sub(*((struct timespec *)(&end_time->ts)), *((struct timespec *)(&start_time->ts)));
}

/**
 * osa_timespec_to_ns - Convert timespec to nanoseconds
 * @ts_args:     pointer to the variable to be converted
 *
 * Returns the given time in nanosecond
 */
int64 osa_timespec_to_ns(struct osa_timespec *ts_args)
{
    struct timespec *ts = (struct timespec *)(&ts_args->ts);
    return ((int64) ts->tv_sec * NSEC_PER_SEC) + ts->tv_nsec;
}

