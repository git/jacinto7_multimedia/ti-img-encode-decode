// SPDX-License-Identifier: GPL-2.0
/*
 * osa string handling functions implementation for Linux
 *
 * Copyright (c) 2019 Texas Instruments Incorporated - http://www.ti.com/
 */

#include <linux/string.h>

#include "../../inc/osa_string.h"

void *osa_memcpy(void *dst, void *src, uint32 bytes)
{
	return memcpy(dst, src, bytes);
}

void *osa_memset(void *ptr, uint32 val, uint32 bytes)
{
	return memset(ptr, val, bytes);
}

void *osa_memmove(void *dst, void *src, uint32 bytes)
{
	return memmove(dst, src, bytes);
}

int osa_strcmp(const osa_char *str1, const osa_char *str2)
{
	return strcmp(str1, str2);
}

int osa_strncmp(const osa_char *str1, const osa_char *str2, uint32 size)
{
	return strncmp(str1, str2, size);
}
