// SPDX-License-Identifier: GPL-2.0
/*
 * osa memory handling functions implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/slab.h>
#include <linux/printk.h>
#include <linux/dma-mapping.h>

#include "../../inc/osa_mem.h"
#include "../../inc/osa_define.h"

#ifdef DEBUG_OSA_MEM
DEFINE_MUTEX(lock);
#define MAX_NUM_RESOURCES 5000
static struct memory_release resource[MAX_NUM_RESOURCES] = {0};
static uint32 count = 0;

void osa_save_mem(void *ptr, uint32 size, const uint8 *func, uint32 line)
{
    uint32 i = 0;

    mutex_lock(&lock);
    for (i = 0; i < MAX_NUM_RESOURCES; i++) {
        if (0 == resource[i].flag) {
            resource[i].flag = 1;
            strcpy(resource[i].func, func);
            resource[i].line = line;
            resource[i].ptr = ptr;
            resource[i].size = size;
            count++;
            mutex_unlock(&lock);
            return;
        }
    }
    mutex_unlock(&lock);
    OSA_PR_ERR("##### NO MEMORY #######\n");
}

void osa_remove_mem(void *ptr, const uint8 *func, uint32 line)
{
    uint32 i = 0;

    mutex_lock(&lock);
    for (i = 0; i < MAX_NUM_RESOURCES; i++) {
        if (1 == resource[i].flag) {
            if (ptr == resource[i].ptr) {
                resource[i].flag = 0;
                count--;
                mutex_unlock(&lock);
                return;
            }
        }
    }
    mutex_unlock(&lock);
    OSA_PR_ERR("##### INVALID ADDRESS ####### [0x%llx] function_name = %s line_no = %d\n",
               (uint64)ptr, func, line);
}

void osa_print_mem_leak()
{
    uint32 i = 0;

    OSA_PR_ERR("##### printing %d outstanding allocations #####\n", count);
    for (i = 0; i < MAX_NUM_RESOURCES; i++) {
        if (1 == resource[i].flag) {
            OSA_PR_ERR("function_name = %s, line_no = %d, ptr = 0x%llx, size = %d\n",
                       resource[i].func, resource[i].line, (uint64)resource[i].ptr,
                       resource[i].size);
        }
    }
}
#endif

#ifdef DEBUG_OSA_MEM
void *osa_calloc_debug(uint64 n, uint64 size, int64 flags, const uint8 *func, uint32 line)
#else
void *osa_calloc(uint64 n, uint64 size, int64 flags)
#endif
{
    void *data = NULL;
    data = kcalloc(n, size, flags);
#ifdef DEBUG_OSA_MEM
    osa_save_mem(data, size * n, func, line);
#endif
    return data;
}

#ifdef DEBUG_OSA_MEM
void *osa_malloc_debug(uint64 size, int64 flags, const uint8 *func, uint32 line)
#else
void *osa_malloc(uint64 size, int64 flags)
#endif
{
    void *data = NULL;
    data = kmalloc(size, flags);
#ifdef DEBUG_OSA_MEM
    osa_save_mem(data, size, func, line);
#endif
    return data;
}

void *osa_realloc(const void *p, uint64 new_size, int64 flags)
{
    return krealloc(p, new_size, flags);
}

#ifdef DEBUG_OSA_MEM
void osa_free_debug(const void *objp, const uint8 *func, uint32 line)
#else
void osa_free(const void *objp)
#endif
{
#ifdef DEBUG_OSA_MEM
    osa_remove_mem((void *)objp, func, line);
#endif
    return kfree(objp);
}

#ifdef DEBUG_OSA_MEM
void *osa_zalloc_debug(uint64 size, int64 flags, const uint8 *func, uint32 line)
#else
void *osa_zalloc(uint64 size, int64 flags)
#endif
{
    void *data = NULL;
    data = kzalloc(size, flags);
#ifdef DEBUG_OSA_MEM
    osa_save_mem(data, size, func, line);
#endif
    return data;
}

#ifdef DEBUG_OSA_MEM
void *osa_malloc_array_debug(size_t n, size_t size, int64 flags, const uint8 *func, uint32 line)
#else
void *osa_malloc_array(size_t n, size_t size, int64 flags)
#endif
{
    void *data = NULL;
    data = kmalloc_array(n, size, flags);
#ifdef DEBUG_OSA_MEM
    osa_save_mem(data, n * size, func, line);
#endif
    return data;
}

#ifdef DEBUG_OSA_MEM
int8 *osa_strdup_debug(const int8 *s, int64 flags, const uint8 *func, uint32 line)
#else
int8 *osa_strdup(const int8 *s, int64 flags)
#endif
{
    void *data = NULL;
    data = kstrdup(s, flags);
#ifdef DEBUG_OSA_MEM
    osa_save_mem(data, strlen(s), func, line);
#endif
    return data;
}

#ifdef DEBUG_OSA_MEM
void *osa_memdup_debug(const void *src, size_t len, int64 flags, const uint8 *func, uint32 line)
#else
void *osa_memdup(const void *src, size_t len, int64 flags)
#endif
{
    void *data = NULL;
    data = kmemdup(src, len, flags);
#ifdef DEBUG_OSA_MEM
    osa_save_mem(data, len, func, line);
#endif
    return data;
}

void *osa_vmap(void **pages, uint32 count)
{
    pgprot_t prot;

    prot = PAGE_KERNEL;
    prot = pgprot_writecombine(prot);

    return vmap((struct page **)pages, count, VM_MAP, prot);
}

void osa_vunmap(const void *addr)
{
    vunmap(addr);
}
