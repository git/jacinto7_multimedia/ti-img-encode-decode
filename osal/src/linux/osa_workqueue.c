// SPDX-License-Identifier: GPL-2.0
/*
 * osa work queue handling for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/slab.h>
#include <linux/printk.h>
#include <linux/mutex.h>

#include "../../inc/osa_workqueue.h"
#include "../../inc/osa_mem.h"

DEFINE_MUTEX(mutex);

struct node
{
    void **key;
    struct node *next;
};

struct node *work_head = NULL;
struct node *delayed_work_head = NULL;

//insert link at the first location
void osa_insertfirst(struct node **head, void **key)
{
    //create a link
    struct node *link = (struct node*) osa_malloc(sizeof(struct node), GFP_KERNEL);
    
    link->key = key;
    mutex_lock(&mutex);
    //point it to old first node
    link->next = *head;

    //point first to new first node
    *head = link;
    mutex_unlock(&mutex);
}

//delete a link with given key
struct node *osa_delete_work_key(void *key, int8 flag)
{
    struct node *temp = NULL;
    struct node *previous = NULL;
    struct work_struct **work = NULL;

    //start from the first link
    mutex_lock(&mutex);
    temp = work_head;

    //if list is empty
    if (work_head == NULL)
    {
        mutex_unlock(&mutex);
        return NULL;
    }

    work = ((struct work_struct **)(temp->key));
    //navigate through list
    while (*work != key)
    {
        //if it is last node
        if (temp->next == NULL)
        {
            mutex_unlock(&mutex);
            return NULL;
        }
        else
        {
            //store reference to current link
            previous = temp;
            //move to next link
            temp = temp->next;
            work = ((struct work_struct **)(temp->key));
        }
    }

    if(flag == TRUE)
    {
        //found a match, update the link
        if (temp == work_head)
        {
            //change first to point to next link
            work_head = work_head->next;
        }
        else
        {
            //bypass the current link
            previous->next = temp->next;
        }
    }

    mutex_unlock(&mutex);
    return temp;
}

//delete a link with given key
struct node *osa_delete_delayed_work_key(void *key, int8 flag)
{
    struct node *temp = NULL;
    struct node *previous = NULL;
    struct delayed_work **dwork = NULL;

    if(flag == TRUE)
    {
        /* This Condition is true when kernel module is removed */
        return delayed_work_head;
    }
    //start from the first link
    mutex_lock(&mutex);
    temp = delayed_work_head;

    //if list is empty
    if (delayed_work_head == NULL)
    {
        mutex_unlock(&mutex);
        return NULL;
    }

    dwork = ((struct delayed_work **)(temp->key));
    //navigate through list
    while (&(*dwork)->work != key)
    {
        //if it is last node
        if (temp->next == NULL)
        {
            mutex_unlock(&mutex);
            return NULL;
        }
        else
        {
            //store reference to current link
            previous = temp;
            //move to next link
            temp = temp->next;
            dwork = ((struct delayed_work **)(temp->key));
        }
    }

    mutex_unlock(&mutex);
    return temp;
}

void *osa_create_workqueue(void *name)
{
    return create_workqueue((int8 *)name);
}


void osa_destroy_workqueue(void *wq)
{
    return destroy_workqueue(wq);
}


void osa_init_work(void **work_args, void *work_fn, uint8_t hwa_id)
{
    struct work_struct **work = (struct work_struct **)work_args;
    *work = osa_zalloc(sizeof(struct work_struct), GFP_KERNEL);
    if(NULL == *work)
    {
        pr_err("Memory allocation failed for work_queue\n");
        return;
    }
    INIT_WORK(*work, work_fn);
    osa_insertfirst(&work_head, (void **)work);
}


void osa_init_delayed_work(void **work_args, void *work_fn, uint8_t hwa_id)
{
    struct delayed_work **work = (struct delayed_work **)work_args;
    *work = osa_zalloc(sizeof(struct delayed_work), GFP_KERNEL);
    if(NULL == *work)
    {
        pr_err("Memory allocation failed for delayed_work_queue\n");
        return;
    }
    INIT_DELAYED_WORK(*work, work_fn);
    osa_insertfirst(&delayed_work_head, (void **)work);
}


int32 osa_queue_work(void *wq, void *work)
{
    return queue_work(wq, work);
}


int32 osa_queue_work_on(int32 cpu, void *wq, void *work)
{
    return queue_work_on(cpu, wq, work);
}


int32 osa_queue_delayed_work(void *wq, void *dwork, uint64 delay)
{
    return queue_delayed_work(wq, dwork, delay);
}


int32 osa_queue_delayed_work_on(int32 cpu, void *wq,
            void *dwork, uint64 delay)
{
    return queue_delayed_work_on(cpu, wq, dwork, delay);
}


int32 osa_schedule_work(void *work)
{
    return schedule_work(work);
}


int32 osa_schedule_work_on(int32 cpu, void *work)
{
    return schedule_work_on(cpu, work);
}


int32 osa_schedule_delayed_work(void *dwork, uint64 delay)
{
    return schedule_delayed_work(dwork, delay);
}


int32 osa_schedule_delayed_work_on(int32 cpu, void *dwork, uint64 delay)
{
    return schedule_delayed_work_on(cpu, dwork, delay);
}


int32 osa_flush_work(void *work)
{
    return flush_work(work);
}


void osa_flush_workqueue(void *wq)
{
    return flush_workqueue(wq);
}


void osa_drain_workqueue(void *wq)
{
    return drain_workqueue(wq);
}


void osa_flush_scheduled_work(void)
{
    return flush_scheduled_work();
}


int32 osa_cancel_delayed_work_sync(void *dwork)
{
    return cancel_delayed_work_sync(dwork);
}

int32 osa_cancel_delayed_work(void *dwork)
{
    return cancel_delayed_work(dwork);
}

int32 osa_cancel_work_sync(void *work)
{
    return cancel_work_sync(work);
}

int32 osa_mod_delayed_work(void*wq, void *dwork, uint64 delay)
{
    return mod_delayed_work(wq, dwork, delay);
}


int32 osa_mod_delayed_work_on(int32 cpu, void *wq,
            void *dwork, uint64 delay)
{
    return mod_delayed_work_on(cpu, wq, dwork, delay);
}

void *osa_get_global_workqueue_struct(void)
{
    return system_wq;
}

void *osa_get_work_buff(void *key, int8 flag)
{
    struct node *data = NULL;
    void *work = NULL;

    data = osa_delete_work_key(key, flag);
    if (NULL != data) {
        work = data->key;
        if (flag == TRUE)
            osa_free(data);
    }
    return work;
}

void *osa_get_delayed_work_buff(void *key, int8 flag)
{
    struct node *data = NULL;
    void *dwork = NULL;

    data = osa_delete_delayed_work_key(key, flag);
    if (NULL != data) {
        dwork = data->key;
        if (flag == TRUE)
            osa_free(data);
    }
    return dwork;
}
