// SPDX-License-Identifier: GPL-2.0
/*
 * osa platform driver specific handling for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/platform_device.h>

#include "../../inc/osa_platform.h"

void *osa_platform_get_drvdata(void *dev)
{
    return platform_get_drvdata(to_platform_device(dev));
}

void *osa_dev_get_drvdata(void *dev)
{
	return ((const struct device*)dev)->driver_data;
}
