// SPDX-License-Identifier: GPL-2.0
/*
 * osa interrupt handling implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/interrupt.h>

#include "../../inc/osa_interrupt.h"

static ulong osa_get_irq_flags(osa_irq_flag_t flags)
{
    ulong irq_flag = IRQF_SHARED;

    switch(flags)
    {
    case OSA_IRQF_SHARED:
        irq_flag = IRQF_SHARED;
        break;
    case OSA_IRQF_PROBE_SHARED:
        irq_flag = IRQF_PROBE_SHARED;
        break;
    case OSA___IRQF_TIMER:
        irq_flag = __IRQF_TIMER;
        break;
    case OSA_IRQF_PERCPU:
        irq_flag = IRQF_PERCPU;
        break;
    case OSA_IRQF_NOBALANCING:
        irq_flag = IRQF_NOBALANCING;
        break;
    case OSA_IRQF_IRQPOLL:
        irq_flag = IRQF_IRQPOLL;
        break;
    case OSA_IRQF_ONESHOT:
        irq_flag = IRQF_ONESHOT;
        break;
    case OSA_IRQF_NO_SUSPEND:
        irq_flag = IRQF_NO_SUSPEND;
        break;
    case OSA_IRQF_FORCE_RESUME:
        irq_flag = IRQF_FORCE_RESUME;
        break;
    case OSA_IRQF_NO_THREAD:
        irq_flag = IRQF_NO_THREAD;
        break;
    case OSA_IRQF_EARLY_RESUME:
        irq_flag = IRQF_EARLY_RESUME;
        break;
    case OSA_IRQF_COND_SUSPEND:
        irq_flag = IRQF_COND_SUSPEND;
        break;
    default:
        pr_err("Invalid osa_irq_flag_t flag input\n");
        break;
    }
    return irq_flag;
}

int32 osa_request_irq(uint32 irq, void *handler,
        osa_irq_flag_t flags, const int8 *name, void *dev)
{
    return request_irq(irq, (irq_handler_t)handler,
            osa_get_irq_flags(flags), name, dev);
}
const void* osa_free_irq(unsigned int irq, void *dev_id)
{
    return free_irq(irq, dev_id);
}
void osa_disable_irq(uint32 irq)
{
    disable_irq(irq);
}

void osa_enable_irq(uint32 irq)
{
    enable_irq(irq);
}

void osa_disable_irq_nosync(uint32 irq)
{
    disable_irq_nosync(irq);
}

void osa_synchronize_irq(uint32 irq)
{
    synchronize_irq(irq);
}

int32 osa_request_threaded_irq(uint32 irq, void *handler,
                               void *thread_fn, osa_irq_flag_t irqflags,
             const uint8 *devname, void *dev_id)
{
    return request_threaded_irq(irq, (irq_handler_t)handler,
            (irq_handler_t)thread_fn,
            osa_get_irq_flags(irqflags), devname, dev_id);
}

int32 osa_devm_request_threaded_irq(void *dev, uint32 irq,
                                    void *handler, void *thread_fn,
                  osa_irq_flag_t irqflags, const uint8 *devname,
                  void *dev_id)
{
    return devm_request_threaded_irq(dev, irq, (irq_handler_t)handler,
            (irq_handler_t)thread_fn,
            osa_get_irq_flags(irqflags), devname, dev_id);
}

void osa_devm_free_irq(void *dev, uint32 irq, void *dev_id)
{
    devm_free_irq(dev, irq, dev_id);
}

void osa_irq_wake_thread(uint32 irq, void *dev_id)
{
    irq_wake_thread(irq, dev_id);
}

int32 osa_request_any_context_irq(uint32 irq, void *handler,
        osa_irq_flag_t flags, const int8 *name, void *dev_id)
{
    return request_any_context_irq(irq, (irq_handler_t)handler,
            osa_get_irq_flags(flags), name, dev_id);
}

int32 osa_request_percpu_irq(uint32 irq, void *handler,
           const int8 *devname, void *percpu_dev_id)
{
    return request_percpu_irq(irq, (irq_handler_t)handler, devname,
            percpu_dev_id);
}

void osa_free_percpu_irq(uint32 irq, void *dev_id)
{
    free_percpu_irq(irq, dev_id);
}

int32 osa_devm_request_irq(void *dev, uint32 irq, void *handler,
        osa_irq_flag_t irqflags, const int8 *devname, void *dev_id)
{
    return devm_request_irq(dev, irq, (irq_handler_t)handler,
            osa_get_irq_flags(irqflags), devname, dev_id);
}

int32 osa_devm_request_any_context_irq(void *dev, uint32 irq,
                                       void *handler, osa_irq_flag_t irqflags,
                  const int8 *devname, void *dev_id)
{
    return devm_request_any_context_irq(dev, irq, (irq_handler_t)handler,
            osa_get_irq_flags(irqflags), devname, dev_id);
}

int32 osa_disable_hardirq(uint32 irq)
{
    return disable_hardirq(irq);
}

void osa_disable_percpu_irq(uint32 irq)
{
    disable_percpu_irq(irq);
}

void osa_enable_percpu_irq(uint32 irq, uint32 type)
{
    enable_percpu_irq(irq, type);
}

int32 osa_irq_percpu_is_enabled(uint32 irq)
{
    return irq_percpu_is_enabled(irq);
}

int32 osa_irq_set_irq_wake(uint32 irq, uint32 on)
{
    return irq_set_irq_wake(irq, on);
}

void osa_local_irq_disable(uint32 *status)
{
    local_irq_disable();
}

void osa_local_irq_enable(uint32 status)
{
    local_irq_enable();
}
