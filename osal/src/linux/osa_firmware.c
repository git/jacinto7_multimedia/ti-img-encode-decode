// SPDX-License-Identifier: GPL-2.0
/*
 * osa firware request functions implementation for Linux
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/firmware.h>
#include <linux/completion.h>
#include <linux/slab.h>

#include "../../inc/osa_firmware.h"
#include "../../inc/osa_mem.h"

void osa_init_completion(void **firmware_loading_complete_args)
{
    struct completion ** firmware_loading_complete =
            (struct completion **) firmware_loading_complete_args;

    *firmware_loading_complete = osa_malloc(sizeof(struct completion), GFP_KERNEL);
    if(NULL == *firmware_loading_complete)
    {
        pr_err("Memory allocation failed for osa_init_completion\n");
        return;
    }
    init_completion(*firmware_loading_complete);
}

int32 osa_request_firmware_nowait(void *module, const uint8 *name,
        void *device, void *context, void *fp)
{
    return request_firmware_nowait(module, FW_ACTION_HOTPLUG, name, device,
            GFP_KERNEL, context, fp);
}

void osa_complete_all(void *x)
{
    complete_all(x);
}

void osa_wait_for_completion(void *x)
{
    wait_for_completion(x);
}

void osa_release_firmware(void *fw)
{
    release_firmware(fw);
}

size_t osa_get_fw_size(void *fw_args)
{
    struct firmware *fw = (struct firmware *)fw_args;
    return fw->size;
}

const uint8 *osa_get_fw_data(void *fw_args)
{
    struct firmware *fw = (struct firmware *)fw_args;
    return fw->data;
}
