ifeq ($(TARGET_PLATFORM),J7)
ifeq ($(TARGET_OS),SYSBIOS)

include $(PRELUDE)
TARGET      := video_codec_decoder_tirtos
TARGETTYPE  := library

IDIRS       += $(VIDEO_CODEC_PATH)/examples
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/driver/common
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/driver/decoder
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/osal/inc
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/timmlib/include
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/timmlib/decoder

include $($(_MODULE)_SDIR)/../concerto_config_inc.mak

DECODE_FILES_PATH = ../../driver/decoder
TIMMLIB_FILES_PATH = ../../timmlib/decoder

CSOURCES    := \
			$(DECODE_FILES_PATH)/bspp.c \
			$(DECODE_FILES_PATH)/core.c \
			$(DECODE_FILES_PATH)/decoder.c \
			$(DECODE_FILES_PATH)/dec_resources.c \
			$(DECODE_FILES_PATH)/h264_secure_parser.c \
			$(DECODE_FILES_PATH)/hevc_secure_parser.c \
			$(DECODE_FILES_PATH)/hw_control.c \
			$(DECODE_FILES_PATH)/pixel_api.c \
			$(DECODE_FILES_PATH)/swsr.c \
			$(DECODE_FILES_PATH)/translation_api.c \
			$(DECODE_FILES_PATH)/vdecdd_utils.c \
			$(DECODE_FILES_PATH)/vdecdd_utils_buf.c \
			$(DECODE_FILES_PATH)/vdec_mmu_wrapper.c \
			$(DECODE_FILES_PATH)/vxd_core.c \
			$(DECODE_FILES_PATH)/vxd_int.c \
			$(DECODE_FILES_PATH)/vxd_pvdec.c \
			$(DECODE_FILES_PATH)/vxd_dec.c \
			$(TIMMLIB_FILES_PATH)/mm_dec_create.c \
			$(TIMMLIB_FILES_PATH)/mm_dec_init.c \
			$(TIMMLIB_FILES_PATH)/mm_dec_priv.c \
			$(TIMMLIB_FILES_PATH)/mm_dec_process.c \

include $(FINALE)

endif
endif
