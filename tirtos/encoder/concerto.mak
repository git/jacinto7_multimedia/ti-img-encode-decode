ifeq ($(TARGET_PLATFORM),J7)
ifeq ($(TARGET_OS),SYSBIOS)

include $(PRELUDE)
TARGET      := video_codec_encoder_tirtos
TARGETTYPE  := library

IDIRS       += $(VIDEO_CODEC_PATH)
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/driver/common
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/driver/encoder
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/osal/inc
IDIRS	    += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/driver/decoder
IDIRS	    += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/timmlib/include
IDIRS	    += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/timmlib/decoder

CFLAGS	    += -DOSAL_SYSBIOS
CFLAGS	    += -DSOC_J721E

include $($(_MODULE)_SDIR)/../concerto_config_inc.mak

COMMON_FILES_PATH = ../../driver/common
ENCODE_FILES_PATH = ../../driver/encoder
OSAL_FILES_PATH   = ../../osal/src/tirtos
TIMMLIB_FILES_PATH = ../../timmlib/encoder

CSOURCES    := 	$(ENCODE_FILES_PATH)/vxe_enc.c \
			$(ENCODE_FILES_PATH)/topaz_device.c \
			$(ENCODE_FILES_PATH)/topazmmu.c \
			$(ENCODE_FILES_PATH)/topaz_api.c \
			$(ENCODE_FILES_PATH)/topaz_api_utils.c \
			$(ENCODE_FILES_PATH)/header_gen.c \
			$(ENCODE_FILES_PATH)/mtx_fwif.c \
			$(TIMMLIB_FILES_PATH)/mm_enc_create.c\
			$(TIMMLIB_FILES_PATH)/mm_enc_init.c\
			$(TIMMLIB_FILES_PATH)/mm_enc_process.c\
			$(TIMMLIB_FILES_PATH)/mm_enc_priv.c \

include $(FINALE)

endif
endif
