ifeq ($(TARGET_PLATFORM),J7)
ifeq ($(TARGET_OS),SYSBIOS)

CFLAGS	    += -DOSAL_SYSBIOS
CFLAGS	    += -DSOC_J721E

# Optional Video feature configuration control

# (1)
# This config allows enabling or disabling of HEVC/H265 video
# decoding functionality with IMG VXD Video decoder. If you
# do not want HEVC decode capability, select N.
# If unsure, select Y
HAS_HEVC ?=y

# (2)
# This config enables error concealment with gray pattern.
# Disable if you do not want error concealment capability.
# If unsure, say Y
ERROR_CONCEALMENT ?=y

# (3)
# This config, if enabled, configures H264 video decoder to
# output frames in the decode order with no buffering and
# picture reordering inside codec.
# If unsure, say N
REDUCED_DPB_NO_PIC_REORDERING ?=y

# (4)
# This config, if enabled, enables all the debug traces in
# decoder driver. Enable it only for debug purpose
# Keep it always disabled for release codebase
DEBUG_DECODER_DRIVER ?=n

# (5)
# This config enables debugging of memory allocations through the
# osa mem module.
# If unsure, say N
DEBUG_OSA_MEM ?=n

# (6)
# This config enables profiling of encoder for encode latency at FW and TIMM lib level.
# If unsure, say N
ENABLE_PROFILING ?=n

# (7)
# This config, if enabled, enables all the debug traces in
# decoder driver. Enable it only for debug purpose
# Keep it always disabled for release codebase
DEBUG_ENCODER_DRIVER ?=n

ifeq ($(DEBUG_ENCODER_DRIVER), y)
ccflags-y   += -DDEBUG_ENCODER_DRIVER
ccflags-y   += -DDEBUG
endif


ifeq ($(HAS_HEVC),y)
CFLAGS   += -DHAS_HEVC
endif

ifeq ($(ERROR_CONCEALMENT),y)
CFLAGS   += -DERROR_CONCEALMENT
endif

ifeq ($(REDUCED_DPB_NO_PIC_REORDERING),y)
CFLAGS   += -DREDUCED_DPB_NO_PIC_REORDERING
endif

ifeq ($(DEBUG_DECODER_DRIVER), y)
CFLAGS   += -DDEBUG_DECODER_DRIVER
CFLAGS   += -DDEBUG
endif

ifeq ($(DEBUG_OSA_MEM),y)
CFLAGS   += -DDEBUG_OSA_MEM
endif

ifeq ($(ENABLE_PROFILING),y)
CFLAGS   += -DENABLE_PROFILING
endif

endif
endif
