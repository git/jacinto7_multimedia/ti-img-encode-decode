// SPDX-License-Identifier: GPL-2.0
/*
 * IMG ENC TIMMLIB Interface function implementations
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2019 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "vxe_enc.h"
#include "osal/inc/osa_mem.h"
#include "osal/inc/osa_idr.h"
#include "osal/inc/osa_err.h"
#include "osal/inc/osa_define.h"
#include "mm_enc_priv.h"
#include "mm_common.h"

struct MM_ENC_Inst g_MM_ENC_Inst = {0};
struct MM_COMMON_Inst g_MM_COMMON_Inst_Enc = {0};

struct vxe_enc_fmt vxe_enc_formats[] = {
	{
		.fourcc = MM_PIX_FMT_NV12,
		.num_planes = 1,
		.type = IMG_ENC_FMT_TYPE_OUTPUT,
		.fmt = IMG_CODEC_420_PL12,
		.min_bufs = 2,
		.size_num[0] = 3,
		.size_den[0] = 2,
		.bytes_pp = 1,
		.csc_preset = IMG_CSC_NONE,
	},
	{
		.fourcc = MM_PIX_FMT_NV12M,
		.num_planes = 2,
		.type = IMG_ENC_FMT_TYPE_OUTPUT,
		.fmt = IMG_CODEC_420_PL12,
		.min_bufs = 2,
		.size_num[0] = 1,
		.size_den[0] = 1,
		.size_num[1] = 1,
		.size_den[1] = 2,
		.bytes_pp = 1,
		.csc_preset = IMG_CSC_NONE,
	},
	{
		.fourcc = MM_PIX_FMT_H264,
		.num_planes = 1,
		.type = IMG_ENC_FMT_TYPE_CAPTURE,
		.std = IMG_STANDARD_H264,
		.min_bufs = 1,
		.size_num[0] = 1,
		.size_den[0] = 1,
		.bytes_pp = 1,
		.csc_preset = IMG_CSC_NONE,
	},
};

struct vxe_enc_fmt *vxe_find_format(struct mm_vxe_enc_fmt *f, uint32 type)
{
	int32 i;

	for (i = 0; i < ARRAY_SIZE(vxe_enc_formats); ++i) {
		if (vxe_enc_formats[i].fourcc == f->pixelformat &&
		    vxe_enc_formats[i].type == type)
			return &vxe_enc_formats[i];
	}
	return NULL;
}

struct vxe_enc_q_data *vxe_get_q_data(struct vxe_enc_ctx *ctx, uint32 type)
{
	switch (type) {
	case MM_BUF_TYPE_VIDEO_OUTPUT:
		return &ctx->q_data[Q_ENC_DATA_SRC];
	case MM_BUF_TYPE_VIDEO_INPUT:
		return &ctx->q_data[Q_ENC_DATA_DST];
	default:
		return NULL;
	}
}

struct vxe_buffer *vxe_get_buf(void *buf, uint32_t chId)
{
	mm_vxe_buff *mm_buf = g_MM_ENC_Inst.ch_obj[chId].mm_buf;
	int i = 0;

	for(i = 0; i <= mm_buf->vxe_buf_index; i++)
	{
		if(buf == mm_buf->vxe_buf[i]->buffer)
		{
			return mm_buf->vxe_buf[i];
		}
	}
	return NULL;
}
