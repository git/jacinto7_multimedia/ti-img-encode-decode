/* SPDX-License-Identifier: GPL-2.0 */
/*
 * decoder interface definitions
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/**
 *  \file     mm_dec.h
 *
 *  \brief    This file contains interface header for TI MM API decoder
 *
 */

#ifndef MM_DEC_H_
#define MM_DEC_H_

#include "mm_common.h"


/** @enum mapper::mm_dec_process_cb
 *  @brief callbacks from codec library
 *  @var mapper::mm_dec_process_cb::MM_CB_STRUNIT_PROCESSED
 *  0
 *  @var mapper::mm_dec_process_cb::MM_CB_SPS_RELEASE
 *  1
 *  @var mapper::mm_dec_process_cb::MM_CB_PPS_RELEASE
 *  2
 *  @var mapper::mm_dec_process_cb::MM_CB_PICT_DECODED
 *  3
 *  @var mapper::mm_dec_process_cb::MM_CB_PICT_DISPLAY
 *  4
 *  @var mapper::mm_dec_process_cb::MM_CB_PICT_RELEASE
 *  5
 *  @var mapper::mm_dec_process_cb::MM_CB_PICT_END
 *  6
 *  @var mapper::mm_dec_process_cb::MM_CB_STR_END
 *  7
 *  @var mapper::mm_dec_process_cb::MM_CB_ERROR_FATAL
 *  8
 *  @var mapper::mm_dec_process_cb::MM_CB_FORCE32BITS
 *  0x7FFFFFFFU
 */
/**
 * \verbatim
 *  MM Decoder supported callback types
 *  Application shall use this type to free/display input/output buffers \endverbatim
 */
typedef enum {
	MM_CB_STRUNIT_PROCESSED,
	MM_CB_SPS_RELEASE,
	MM_CB_PPS_RELEASE,
	MM_CB_PICT_DECODED,
	MM_CB_PICT_DISPLAY,
	MM_CB_PICT_RELEASE,
	MM_CB_PICT_END,
	MM_CB_STR_END,
	MM_CB_ERROR_FATAL,
	MM_CB_FORCE32BITS = 0x7FFFFFFFU
} mm_dec_process_cb;

typedef struct {
	/** Placeholder for empty struct */
	uint32_t placeholder;
} mm_dec_ctrl_params;

/**
 *  \brief MM_DEC_Init - Initialize the decoder
 *  This function should called only once.
 *  \verbatim
 *  Return: 0          -> Success
 *  Return: -ve values -> Failure \endverbatim
 */
int32_t MM_DEC_Init(void);

/**
 *  \brief MM_DEC_Create - Create the decoder instance (channel specific API)
 *  \verbatim
 *  params: MM Decoder create params
 *  chId  : Channel ID
 *  Return: 0 -> Success
 *  Return:-1 -> Failure \endverbatim
 */
int32_t MM_DEC_Create(mm_vid_create_params *params, mm_dec_ctrl_params *ctrl, uint32_t *chId);

/**
 *  \brief MM_DEC_RegisterCb - Register Callback (channel specific API)
 *  This is the callback function when decoder completes frame decoding
 *  \verbatim
 *  buf     : Decoder output buffer (frame buffer)
 *  cb_type : callback Type
 *  chId	: Channel ID
 *  Return  : 0 -> Success
 *  Return  :-1 -> Failure \endverbatim
 */
int32_t MM_DEC_RegisterCb(
		void (*mm_ret_resource)(struct mm_buffer *buf, mm_dec_process_cb cb_type),
		uint32_t chId);

/**
 *  \brief MM_DEC_StartStreaming - Start the instance (channel specific API)
 *  \verbatim
 *  chId  : Channel ID
 *  type  : buffer type, input or output
 *  Return: 0 -> Success
 *  Return:-1 -> Failure \endverbatim
 */
int32_t MM_DEC_StartStreaming(uint32_t chId, mm_buffertype type);

/**
 *  \brief MM_DEC_BufPrepare - Alloc/Prepare the decoder buffers (channel specific API)
 *  This needs to be called for all input and output buffers atleast
 *  once to map the buffer with VXD MMU.
 *  \verbatim
 *  buffer: Buffer Object
 *  chId  : Channel ID
 *  Return: 0 -> Success
 *  Return:-1 -> Failure \endverbatim
 */
int32_t MM_DEC_BufPrepare(struct mm_buffer *buffer, uint32_t chId);

/**
 *  \brief MM_DEC_Process - Perform the decode Process (channel specific API)
 *  Non-blocking API, Decode completion is notified by callback
 *  \verbatim
 *  in_buf  : Input Buffer Object
 *  out_buf : Output Buffer Object
 *  chId    : Channel ID
 *  Return  : 0 -> Success
 *  Return  :-1 -> Failure\endverbatim
 */
int32_t MM_DEC_Process(struct mm_buffer *in_buf,
		struct mm_buffer *out_buf, uint32_t chId);

/**
 *  \brief MM_DEC_StopStreaming - Stop the instance (channel specific API)
 *  \verbatim
 *  chId  : Channel ID
 *  type  : buffer type, input or output
 *  Return: 0 -> Success
 *  Return:-1 -> Failure \endverbatim
 */
int32_t MM_DEC_StopStreaming(uint32_t chId, mm_buffertype type);

/**
 *  \brief MM_DEC_Destroy - Destroy the decoder instance (channel specific API)
 *  \verbatim
 *  chId  : Channel ID
 *  Return: 0 -> Success
 *  Return:-1 -> Failure \endverbatim
 */
int32_t MM_DEC_Destroy(uint32_t chId);

/**
 *  \brief MM_DEC_Deinit - De-initialize the decoder
 *  This function should called only once.
 *  \verbatim
 *  Return: 0 -> Success
 *  Return:-1 -> Failure \endverbatim
 */
int32_t MM_DEC_Deinit(void);

/*@}*/
#endif /* MM_DEC_H_ */
