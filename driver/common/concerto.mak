ifeq ($(TARGET_PLATFORM),J7)
ifeq ($(TARGET_OS),SYSBIOS)

include $(PRELUDE)
TARGET      := video_codec_common_tirtos
TARGETTYPE  := library

IDIRS       += $(VIDEO_CODEC_PATH)/examples
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/driver/common
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/driver/decoder
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/osal/inc
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/timmlib/include
IDIRS       += $(VIDEO_CODEC_PATH)/ti-img-encode-decode/timmlib/decoder

include $($(_MODULE)_SDIR)/../../tirtos/concerto_config_inc.mak

COMMON_FILES_PATH =

CSOURCES    := \
			$(COMMON_FILES_PATH)/dq.c \
			$(COMMON_FILES_PATH)/hash.c \
			$(COMMON_FILES_PATH)/idgen_api.c \
			$(COMMON_FILES_PATH)/lst.c \
			$(COMMON_FILES_PATH)/pool.c \
			$(COMMON_FILES_PATH)/pool_api.c \
			$(COMMON_FILES_PATH)/ra.c \
			$(COMMON_FILES_PATH)/resource.c \
			$(COMMON_FILES_PATH)/rman_api.c \
			$(COMMON_FILES_PATH)/addr_alloc.c \
			$(COMMON_FILES_PATH)/img_mem_man.c \
			$(COMMON_FILES_PATH)/img_mem_unified.c \
			$(COMMON_FILES_PATH)/imgmmu.c \
			$(COMMON_FILES_PATH)/talmmu_api.c \

include $(FINALE)

endif
endif
