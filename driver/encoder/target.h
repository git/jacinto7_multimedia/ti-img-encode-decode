/* SPDX-License-Identifier: GPL-2.0 */
/*
 * target interface header
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#if !defined (__TARGET_H__)
#define __TARGET_H__

#include "osal/inc/osa_types.h"

#define TARGET_NO_IRQ   (999) /* Interrupt number when no interrupt exists */

/*
 * The memory space types
 */
enum mem_space_type
{
	MEMSPACE_REGISTER,  /* Memory space is mapped to device registers */
	MEMSPACE_MEMORY,     /* Memory space is mapped to device memory */
	MEMSPACE_FORCE32BITS = 0x7FFFFFFFU
};

/*
 * This structure contains all information about a device register
 */
struct mem_space_reg
{
	uint64 addr; /* Base address of device registers */
	uint32 size; /* Size of device register block */
	uint32 intr_num; /* The interrupt number */
};

/*
 * This structure contains all information about a device memory region
 */
struct mem_space_mem
{
	uint64 addr; /* Base address of memory region */
	uint64 size; /* Size of memory region */
	uint64 guard_band; /* Memory guard band */
};

/*
 * This structure contains all information about the device memory space
 */
struct mem_space
{
	osa_char *               name;     /* Memory space name */
	enum mem_space_type      type;     /* Memory space type */
	union
	{
		struct mem_space_reg reg;      /* Device register info */
		struct mem_space_mem mem;      /* Device memory region info */
	};

	uint_addr		cpu_addr; /* Cpu KM address for the mem space */
};

struct target_config
{
	uint32 num_mem_spaces;
	struct mem_space * mem_spaces;
};

#endif /* __TARGET_H__    */
