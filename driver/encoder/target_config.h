/* SPDX-License-Identifier: GPL-2.0 */
/*
 * Device specific memory configuration
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __TARGET_CONFIG_H__
#define __TARGET_CONFIG_H__

#include <target.h>

/* Order MUST match with topaz_mem_space definition */
enum topaz_mem_space_idx {
        REG_TOPAZHP_MULTICORE = 0,
        REG_DMAC,
        REG_COMMS,
        REG_MTX,
        REG_MMU,
        REG_TOPAZHP_TEST,
        REG_MTX_RAM,
        REG_TOPAZHP_CORE_0,
        REG_TOPAZHP_VLC_CORE_0,
        REG_TOPAZHP_DEBLOCKER_CORE_0,
        REG_TOPAZHP_COREEXT_0,
        REG_TOPAZHP_CORE_1,
        REG_TOPAZHP_VLC_CORE_1,
        REG_TOPAZHP_DEBLOCKER_CORE_1,
        REG_TOPAZHP_COREEXT_1,
        REG_TOPAZHP_CORE_2,
        REG_TOPAZHP_VLC_CORE_2,
        REG_TOPAZHP_DEBLOCKER_CORE_2,
        REG_TOPAZHP_COREEXT_2,
        REG_TOPAZHP_CORE_3,
        REG_TOPAZHP_VLC_CORE_3,
        REG_TOPAZHP_DEBLOCKER_CORE_3,
        REG_TOPAZHP_COREEXT_3,
        FW,
        SYSMEM,
        MEMSYSMEM,
        MEM,
        FB,
        MEMDMAC_00,
        MEMDMAC_01,
        MEMDMAC_02,
        MEM_SPACE_FORCE32BITS = 0x7FFFFFFFU
};

#endif
