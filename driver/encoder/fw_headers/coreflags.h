/* SPDX-License-Identifier: GPL-2.0 */
/*
 * firmware header
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef _COREFLAGS_H_
#define _COREFLAGS_H_

#define SERIALIZED_PIPES (1)

/* The number of TOPAZ cores present in the system */
#define TOPAZHP_MAX_NUM_PIPES	(4)

#define TOPAZHP_MAX_POSSIBLE_STREAMS (8)
#define TOPAZHP_MAX_BU_SUPPORT_HD	90
#define TOPAZHP_MAX_BU_SUPPORT_4K	128

#define USE_VCM_HW_SUPPORT (1)

#define INPUT_SCALER_SUPPORTED (1)	/* controls the firmwares ability to support the optional hardware input scaler */

#define SECURE_MODE_POSSIBLE (1)		/* controls the firmwares ability to support secure mode firmware upload */



#define SECURE_IO_PORTS (1)		/* controls the firmwares ability to support secure input/output ports */

/* Line counter feature is not ready for Onyx yet (comment the define to remove the feature from builds) */
#define LINE_COUNTER_SUPPORTED (1)


//#define FW_LOGGING (1)

#if defined(TOPAZ_MTX_HW)
#	if defined(ENABLE_FORCED_INLINE)
/*
	__attribute__((always_inline)) should be only used when all C code is compiled by
	GCC to a blob object with `-combine` swithch.
*/
#		define MTX_INLINE  inline __attribute__((always_inline))
#else
#		define MTX_INLINE  inline
#endif
#else
#	define MTX_INLINE
#endif

#endif
