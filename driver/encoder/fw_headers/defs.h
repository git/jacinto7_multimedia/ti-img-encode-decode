/* SPDX-License-Identifier: GPL-2.0 */
/*
 * firmware header
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#if !defined DEFS_H_
#define DEFS_H_

#include "osal/inc/osa_types.h"

/*
 * MACROS to insert values into fields within a word. The basename of the
 * field must have MASK_BASENAME and SHIFT_BASENAME constants.
 */
#define F_MASK(basename)  (MASK_##basename)
#define F_SHIFT(basename) (SHIFT_##basename)
/*
 * Extract a value from an instruction word.
 */
#define F_EXTRACT(val,basename) (((val)&(F_MASK(basename)))>>(F_SHIFT(basename)))

/*
 * Mask and shift a value to the position of a particular field.
 */
#define F_ENCODE(val,basename)  (((val)<<(F_SHIFT(basename)))&(F_MASK(basename)))
#define F_DECODE(val,basename)  (((val)&(F_MASK(basename)))>>(F_SHIFT(basename)))

/*
 * Insert a value into a word.
 */
#define F_INSERT(word,val,basename) (((word)&~(F_MASK(basename))) | (F_ENCODE((val),basename)))

/*
 * Extract a 2s complement value from an word, and make it the correct sign
 * Works by testing the top bit to see if the value is negative
 */
#define F_EXTRACT_2S_COMPLEMENT( value, field ) ((int32)(((((((F_MASK( field ) >> F_SHIFT( field )) >> 1) + 1) & (value >> F_SHIFT( field ))) == 0) ? F_EXTRACT( value, field ) : (-(int32)(((~value & F_MASK( field )) >> F_SHIFT( field )) + 1)))))

/*
 * B stands for 'bitfield', defines should be in the form
 * #define FIELD_NAME 10:8		(i.e. upper : lower, both inclusive)
 */
#define B_EXTRACT( data, bits ) (((data) & ((uint32)0xffffffff >> (31 - (1 ? bits)))) >> (0 ? bits))
#define B_MASK( bits ) ((((uint32)0xffffffff >> (31 - (1 ? bits))) >> (0 ? bits)) << (0 ? bits))
#define B_ENCODE( data, bits ) (((data) << (0 ? bits)) & (B_MASK( bits )))
#define B_INSERT( word, data, bits ) (((word) & ~(B_MASK( bits ))) | (B_ENCODE( data, bits )))

/*
 * B_BIT returns boolean true if the corresponding bit is set
 * defines must be in the form FIELD_NAME 10:8 as above, except
 * that the bitfield must obviously be only 1 bit wide
*/
#define B_BIT( word, bits ) ((((word) >> (0 ?bits)) & 1) == 1)

#endif
