/* SPDX-License-Identifier: GPL-2.0 */
/*
 * firmware header
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef _REGCONV_H_topazhp_multicore_regs_old_h
#define _REGCONV_H_topazhp_multicore_regs_old_h

/* Register CR_LAMBDA_DC_TABLE */
#define TOPAZHP_TOP_CR_LAMBDA_DC_TABLE 0x0120
#define MASK_TOPAZHP_TOP_CR_QPC_OR_DC_SCALE_LUMA_TABLE 0x000000FF
#define SHIFT_TOPAZHP_TOP_CR_QPC_OR_DC_SCALE_LUMA_TABLE 0
#define REGNUM_TOPAZHP_TOP_CR_QPC_OR_DC_SCALE_LUMA_TABLE 0x0120
#define SIGNED_TOPAZHP_TOP_CR_QPC_OR_DC_SCALE_LUMA_TABLE 0

#define MASK_TOPAZHP_TOP_CR_SATD_LAMBDA_OR_DC_SCALE_CHROMA_TABLE 0x0000FF00
#define SHIFT_TOPAZHP_TOP_CR_SATD_LAMBDA_OR_DC_SCALE_CHROMA_TABLE 8
#define REGNUM_TOPAZHP_TOP_CR_SATD_LAMBDA_OR_DC_SCALE_CHROMA_TABLE 0x0120
#define SIGNED_TOPAZHP_TOP_CR_SATD_LAMBDA_OR_DC_SCALE_CHROMA_TABLE 0

#define MASK_TOPAZHP_TOP_CR_SAD_LAMBDA_TABLE 0x007F0000
#define SHIFT_TOPAZHP_TOP_CR_SAD_LAMBDA_TABLE 16
#define REGNUM_TOPAZHP_TOP_CR_SAD_LAMBDA_TABLE 0x0120
#define SIGNED_TOPAZHP_TOP_CR_SAD_LAMBDA_TABLE 0


/* Register CR_MVCALC_COLOCATED (from topazhp_core_regs.h) */
#define MASK_TOPAZHP_CR_TEMPORAL_BLEND 0x001F0000
#define SHIFT_TOPAZHP_CR_TEMPORAL_BLEND 16
#define REGNUM_TOPAZHP_CR_TEMPORAL_BLEND 0x0174
#define SIGNED_TOPAZHP_CR_TEMPORAL_BLEND 0

#endif
