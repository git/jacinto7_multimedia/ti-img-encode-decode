/* SPDX-License-Identifier: GPL-2.0 */
/*
 * topaz utility header
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "osal/inc/osa_types.h"
#include "topaz_api.h"

/*
 * Calculates the correct number of macroblocks per kick and kicks per BU
 */
void calculate_kick_and_bu_size(uint32 width_in_mbs, uint32 height_in_mbs, osa_bool is_interlaced, uint32 max_bu_per_frame,
				uint32 *kick_size, uint32 *kicks_per_bu,  uint32 *min_slice_height);

uint32 calculate_stride(enum img_format format, uint16 requested_stride_bytes, uint16 width);

void topaz_setup_input_format(struct img_video_context *video, struct img_vxe_scaler_setup *scaler_setup);

void topaz_setup_input_csc(struct img_video_context *video, struct img_vxe_scaler_setup *scaler_setup,
			   struct img_vxe_csc_setup *csc_setup, enum img_csc_preset csc_preset);

uint32 topaz_get_packed_buffer_strides(uint16 buffer_stride_bytes, enum img_format format,
				       osa_bool enable_scaler, osa_bool is_interlaced,
				       osa_bool is_interleaved);

void prepare_mv_estimates(struct img_enc_context *enc);

void adjust_pic_flags(struct img_enc_context *enc, struct img_rc_params *prc_params,
		      osa_bool first_pic, uint32 *flags);

void setup_rc_data(struct img_video_context *video, struct pic_params *pic_params,
		   struct img_rc_params *rc_params);

void patch_hw_profile(struct img_video_params *video_params, struct img_video_context *video);
