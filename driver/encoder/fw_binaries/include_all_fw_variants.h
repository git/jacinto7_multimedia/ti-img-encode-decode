/* SPDX-License-Identifier: GPL-2.0 */
/*
 * firmware header
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __INCLUDE_ALL_VARIANTS_INC_INCLUDED__
#define __INCLUDE_ALL_VARIANTS_INC_INCLUDED__

#define INCLUDE_ALL_VARIANTS_TEMPLATE_VERSION (1)

#define FW_BIN_FORMAT_VERSION (2)

struct IMG_COMPILED_FW_BIN_RECORD {

	uint32 text_size, data_size;
	uint32 data_origin, text_origin;
	uint32 text_reloc_size, data_reloc_size;

	uint32 pipes;
	osa_char *fmt, *rc_mode;
	uint32 formats_mask, hw_config;

	uint32 int_define_cnt;
	osa_char **int_define_names;
	uint32 *int_defines;

	uint32 *text, *data;
	uint32 *text_reloc, *data_reloc;
	uint32 *text_reloc_full_addr, *text_reloc_type;
};

#include "ALL_CODECS_FW_ALL_pipes_2_contexts_8_hwconfig_1_bin.c"

uint32 all_fw_binaries_cnt = 1;
struct IMG_COMPILED_FW_BIN_RECORD *all_fw_binaries[] = {
	&sIMG_COMPILED_ALL_CODECS_FW_ALL_pipes_2_contexts_8_hwconfig_1,
};

#endif
