/* SPDX-License-Identifier: GPL-2.0 */
/*
 * VXD Decoder CORE and V4L2 Node Interface header
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __CORE_H__
#define __CORE_H__

#include "osal/inc/osa_types.h"
#include "decoder.h"

int32 core_initialise(void *dev_handle, uint32 internal_heap_id,
		    void *cb);

int32 core_deinitialise(void);

int32 core_supported_features(struct vdec_features *features);

int32 core_stream_create(void *vxd_dec_ctx_arg,
		       const struct vdec_str_configdata *str_cfgdata,
		       uint32 *res_str_id);

int32 core_stream_destroy(uint32 res_str_id);

int32 core_stream_play(uint32 res_str_id);

int32 core_stream_stop(uint32 res_str_id);

int32 core_stream_map_buf(uint32 res_str_id, enum vdec_buf_type buf_type,
			struct vdec_buf_info *buf_info, uint32 *buf_map_id);

int32 core_stream_map_buf_sg(uint32 res_str_id,
			   enum vdec_buf_type buf_type,
			   struct vdec_buf_info *buf_info,
			   void *sgt, uint32 *buf_map_id);

int32 core_stream_unmap_buf(uint32 buf_map_id);

int32 core_stream_unmap_buf_sg(uint32 buf_map_id);

int32 core_stream_submit_unit(uint32 res_str_id,
			    struct vdecdd_str_unit *str_unit);

int32 core_stream_fill_pictbuf(uint32 buf_map_id);

/* This function to be called before stream play */
int32 core_stream_set_output_config(uint32 res_str_id,
				  struct vdec_str_opconfig *str_opcfg,
				  struct vdec_pict_bufconfig *pict_bufcg);

int32 core_stream_flush(uint32 res_str_id, uint8 discard_refs);

int32 core_stream_release_bufs(uint32 res_str_id,
			     enum vdec_buf_type buf_type);

int32 core_stream_get_status(uint32 res_str_id,
			   struct vdecdd_decstr_status *str_status);

#endif
