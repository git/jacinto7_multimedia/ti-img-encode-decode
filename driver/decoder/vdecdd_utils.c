// SPDX-License-Identifier: GPL-2.0
/*
 * VXD Decoder device driver utility functions implementation
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "osal/inc/osa_mem.h"
#include "osal/inc/osa_define.h"
#include "bspp.h"
#include "vdecdd_utils.h"

/*
 * @Function              VDECDDUTILS_FreeStrUnit
 */
int32 vdecddutils_free_strunit(struct vdecdd_str_unit *str_unit)
{
	struct bspp_bitstr_seg *bstr_seg;

	/* Loop over bit stream segments */
	bstr_seg = (struct bspp_bitstr_seg *)
			lst_removehead(&str_unit->bstr_seg_list);
	while (bstr_seg) {
		/* Free segment. */
		osa_free(bstr_seg);

		/* Get next segment. */
		bstr_seg = (struct bspp_bitstr_seg *)
				lst_removehead(&str_unit->bstr_seg_list);
	}

	/* Free the sequence header */
	if (str_unit->seq_hdr_info) {
		str_unit->seq_hdr_info->ref_count--;
		if (str_unit->seq_hdr_info->ref_count == 0) {
			osa_free(str_unit->seq_hdr_info);
			str_unit->seq_hdr_info = NULL;
		}
	}

	/* Free the picture header... */
	if (str_unit->pict_hdr_info) {
		osa_free(str_unit->pict_hdr_info->pict_sgm_data.pic_data);
		str_unit->pict_hdr_info->pict_sgm_data.pic_data = NULL;

		osa_free(str_unit->pict_hdr_info);
		str_unit->pict_hdr_info = NULL;
	}

	/* Free stream unit. */
	osa_free(str_unit);
	str_unit = NULL;

	/* Return success */
	return IMG_SUCCESS;
}

/*
 * @Function: VDECDDUTILS_CreateStrUnit
 * @Description: this function allocate a structure for a complete data unit
 */
int32 vdecddutils_create_strunit(struct vdecdd_str_unit **str_unit_handle,
			       struct lst_t *bs_list)
{
	struct vdecdd_str_unit *str_unit;
	struct bspp_bitstr_seg *bstr_seg;

	str_unit = osa_zalloc(sizeof(*str_unit), OSA_GFP_KERNEL);
	VDEC_ASSERT(str_unit);
	if (!str_unit)
		return IMG_ERROR_OUT_OF_MEMORY;

	if (bs_list) {
		/* copy BS list to this list */
		lst_init(&str_unit->bstr_seg_list);
		for (bstr_seg = lst_first(bs_list); bstr_seg;
		     bstr_seg = lst_first(bs_list)) {
			bstr_seg = lst_removehead(bs_list);
			lst_add(&str_unit->bstr_seg_list, bstr_seg);
		}
	}

	*str_unit_handle = str_unit;

	return IMG_SUCCESS;
}

