/* SPDX-License-Identifier: GPL-2.0 */
/*
 * VDECDD translation API's.
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#ifndef __TRANSLATION_API_H__
#define __TRANSLATION_API_H__

#include "decoder.h"
#include "hw_control.h"
#include "vdecdd_defs.h"
#include "vdec_defs.h"
#include "vxd_props.h"

/*
 * This function submits a stream unit for translation
 * into a control allocation buffer used in PVDEC operation.
 */
int32 translation_ctrl_alloc_prepare
		(struct vdec_str_configdata *psstr_config_data,
		 struct vdecdd_str_unit *psstrunit,
		 struct dec_decpict *psdecpict,
		 const struct vxd_coreprops *core_props,
		 struct decoder_regsoffsets *regs_offset);

/*
 * TRANSLATION_FragmentPrepare.
 */
int32 translation_fragment_prepare(struct dec_decpict *psdecpict,
				 struct lst_t *decpic_seg_list, int32 eop,
				 struct dec_pict_fragment *pict_fragement);

#endif /* __TRANSLATION_API_H__ */
