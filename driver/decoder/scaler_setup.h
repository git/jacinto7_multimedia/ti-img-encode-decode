/* SPDX-License-Identifier: GPL-2.0 */
/*
 * VXD DEC constants calculation and scalling coefficients
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#ifndef _SCALER_SETUP_H
#define _SCALER_SETUP_H

#define LOWP				11
#define HIGHP				14

#define FIXED(a, digits)		((int32)((a) * (1 << (digits))))

struct scaler_params {
	uint32  vert_pitch;
	uint32  vert_startpos;
	uint32  vert_pitch_chroma;
	uint32  vert_startpos_chroma;
	uint32  horz_pitch;
	uint32  horz_startpos;
	uint32  horz_pitch_chroma;
	uint32  horz_startpos_chroma;
	uint8   fixed_point_shift;
};

struct scaler_filter {
	uint8 bhoriz_bilinear;
	uint8 bvert_bilinear;
};

struct scaler_pitch {
	int32 horiz_luma;
	int32 vert_luma;
	int32 horiz_chroma;
	int32 vert_chroma;
};

struct scaler_config {
	enum vdec_vid_std vidstd;
	const struct vxd_coreprops *coreprops;
	struct pixel_pixinfo *in_pixel_info;
	const struct pixel_pixinfo *out_pixel_info;
	uint8 bfield_coded;
	uint8 bseparate_chroma_planes;
	uint32 recon_width;
	uint32 recon_height;
	uint32 mb_width;
	uint32 mb_height;
	uint32 scale_width;
	uint32 scale_height;
};

#endif /* _SCALER_SETUP_H */
