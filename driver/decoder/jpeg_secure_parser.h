/* SPDX-License-Identifier: GPL-2.0 */
/*
 * JPEG secure data unit parsing API.
 *
 * Copyright (c) Imagination Technologies Ltd.
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#ifndef __JPEGSECUREPARSER_H__
#define __JPEGSECUREPARSER_H__

#include "bspp_int.h"

struct bspp_jpeg_sequ_hdr_info {
	uint32 dummy;
};

int32 bspp_jpeg_setparser_config(enum vdec_bstr_format bstr_format,
			       struct bspp_vid_std_features *pvidstd_features,
			       struct bspp_swsr_ctx *pswsr_ctx,
			       struct bspp_parser_callbacks *pparser_callbacks,
			       struct bspp_inter_pict_data *pinterpict_data);

void bspp_jpeg_determine_unit_type(uint8 bitstream_unittype,
				   int32 disable_mvc,
				   enum bspp_unit_type *bspp_unittype);

#endif /*__JPEGSECUREPARSER_H__ */
